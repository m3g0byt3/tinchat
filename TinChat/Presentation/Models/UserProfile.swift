//
//  UserProfile.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

struct UserProfile: Equatable {

    let username: String?
    let description: String?
    let imageBlob: Data?
}

// MARK: - Convenience initializer

extension UserProfile {

    init() {
        self.username = nil
        self.description = nil
        self.imageBlob = nil
    }
}
