//
//  Message.swift
//  TinChat
//
//  Created by m3g0byt3 on 05/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

struct Message: Codable, Equatable, Hashable {

    // MARK: Types

    enum Direction: Int16, Codable {
        case inbound
        case outbound
    }

    enum `Type`: String, Codable {
        case textMessage = "TextMessage"
    }

    // MARK: - Public properties

    let type: Type
    let direction: Direction
    let identifier: String
    let date: Date
    let text: String

    // MARK: - Initialization/Deinitialization

    init(text: String) {
        self.text = text
        self.type = .textMessage
        self.identifier = Message.generateMessageID()
        self.direction = .outbound
        self.date = Date()
    }

    // MARK: - Private API

    private static func generateMessageID() -> String {
        let components = [
            "\(arc4random_uniform(.max))",
            "\(Date.timeIntervalSinceReferenceDate)",
            "\(arc4random_uniform(.max))"
        ]
        // swiftlint:disable:next force_unwrapping
        return components.joined().base64Encoded!
    }
}

// MARK: - Codable protocol conformance

extension Message {
    private enum CodingKeys: String, CodingKey {
        case type = "eventType"
        case identifier = "messageId"
        case text = "text"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.text = try container.decode(String.self, forKey: .text)
        self.type = try container.decode(Type.self, forKey: .type)
        self.identifier = try container.decode(String.self, forKey: .identifier)
        self.direction = .inbound
        self.date = Date()
    }
}
