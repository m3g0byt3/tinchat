//
//  PhotoWrapper.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

// Opaque `UIImage` wrapper used to avoid UIKit imports in service/core layers
struct PhotoWrapper {

    private let imageBlob: Data

    init(imageBlob: Data) {
        self.imageBlob = imageBlob
    }

    var image: UIImage? {
        return UIImage(data: imageBlob)
    }
}
