//
//  Photo.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

struct Photo {
    let url: URL
}

extension Photo: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        self.url = try container.decode(URL.self)
    }
}
