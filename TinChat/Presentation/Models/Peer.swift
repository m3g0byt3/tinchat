//
//  Peer.swift
//  TinChat
//
//  Created by m3g0byt3 on 28/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

struct Peer: Equatable, Hashable {
    enum Status {
        case online
        case offline
    }

    let identifier: String
    let name: String
}
