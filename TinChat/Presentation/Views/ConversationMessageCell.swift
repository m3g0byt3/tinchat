//
//  ConversationMessageCell.swift
//  TinChat
//
//  Created by m3g0byt3 on 06/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

final class ConversationMessageCell: UITableViewCell {

    // MARK: - IBOutlets/UI

    @IBOutlet private weak var messageLabel: UILabel!

    // MARK: - Public API

    override func prepareForReuse() {
        super.prepareForReuse()
        messageLabel.text = nil
    }
}

// MARK: - Configurable protocol conformance

extension ConversationMessageCell: Configurable {
    typealias Model = MessageViewModel

    @discardableResult
    func configure(with model: Model) -> Self {
        messageLabel.text = model.text

        return self
    }
}
