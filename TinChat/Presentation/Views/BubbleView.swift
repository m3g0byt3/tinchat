//
//  BubbleView.swift
//  TinChat
//
//  Created by m3g0byt3 on 06/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
final class BubbleView: UIView {

    // MARK: - Types

    enum Source {
        case left, right
    }

    // MARK: - Public Properties

    var source: Source = .left

    @IBInspectable
    var horizontalInsets: CGFloat = 5.0 {
        didSet {
            setupInsets()
        }
    }

    @IBInspectable
    var verticalInsets: CGFloat = 0.0 {
        didSet {
            setupInsets()
        }
    }

    @IBInspectable
    var cornerRadius: CGFloat = 10.0

    @IBInspectable
    var bubbleColor: UIColor = .green

    // MARK: - Private Properties

    @IBInspectable
    private var isSourceLeft: Bool = true {
        didSet {
            source = isSourceLeft ? .left : .right
        }
    }

    private var sign: CGFloat {
        return source == .left ? 1.0 : -1.0
    }

    private var xAxis: CGFloat {
        return source == .left ? bounds.minX : bounds.maxX
    }

    // MARK: - Public API

    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupInsets()
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        let rect = bounds.insetBy(dx: horizontalInsets, dy: verticalInsets)
        let rectPath = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
        let shapePath = UIBezierPath()

        bubbleColor.setFill()

        rectPath.close()
        rectPath.fill()

        shapePath.move(to: CGPoint(x: xAxis, y: bounds.maxY))
        shapePath.addLine(to: CGPoint(x: xAxis + 50.0 * sign, y: bounds.maxY - 15.0))
        shapePath.addLine(to: CGPoint(x: xAxis + horizontalInsets * sign, y: bounds.maxY - 15.0))

        shapePath.close()
        shapePath.fill()
    }

    // MARK: - Private API

    private func setupUI() {
        contentMode = .redraw
    }

    private func setupInsets() {
        layoutMargins.left += horizontalInsets
        layoutMargins.right += horizontalInsets
        layoutMargins.top += verticalInsets
        layoutMargins.bottom += verticalInsets
    }
}
