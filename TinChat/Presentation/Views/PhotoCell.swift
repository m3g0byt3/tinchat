//
//  PhotoCell.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

final class PhotoCell: UICollectionViewCell {

    // MARK: - IBOutlets/UI

    @IBOutlet private weak var photoImageView: UIImageView!

    // MARK: - Overrides
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.image = R.image.photoPlaceholder()
    }
}
// MARK: - Configurable protocol conformance

extension PhotoCell: Configurable {
    typealias Model = PhotoWrapper

    @discardableResult
    func configure(with model: Model) -> Self {
        photoImageView.image = model.image

        return self
    }
}
