//
//  ConversationsListCell.swift
//  TinChat
//
//  Created by m3g0byt3 on 03/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

final class ConversationsListCell: UITableViewCell {

    // MARK: - IBOutlets/UI

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!

    // MARK: - Public API

    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        dateLabel.text = nil
        messageLabel.text = nil
        backgroundColor = .clear
        messageLabel.font = UIFont.preferredFont(forTextStyle: .body)
    }

    // MARK: - Private API

    private func setupUI() {
        nameLabel.font = UIFont.preferredFont(forTextStyle: .title1, withSymbolicTraits: .traitBold)
    }
}

// MARK: - Configurable protocol conformance

extension ConversationsListCell: Configurable {
    typealias Model = ConversationViewModel

    @discardableResult
    func configure(with model: Model) -> Self {
        nameLabel.text = model.name
        messageLabel.text = model.message
        dateLabel.text = model.date
        backgroundColor = model.isOnline ? R.clr.tinchat.onlineCell() : R.clr.tinchat.offlineCell()
        messageLabel.font = model.hasUnreadMessages
            ? UIFont.preferredFont(forTextStyle: .body, withSymbolicTraits: .traitBold)
            : UIFont.preferredFont(forTextStyle: .body)

        return self
    }
}
