//
//  OverlayPassthroughWindow.swift
//  TinChat
//
//  Created by m3g0byt3 on 02/12/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

/// Just a simple `UIWindow` subclass that will capture and process all events received in `sendEvent(_:)`.
final class OverlayPassthroughWindow: UIWindow {

    // MARK: - Private properties

    private weak var overlayView: OverlayLogoEmittingView?

    // MARK: - Initialization/Deinitialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    // MARK: - Overrides

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func addSubview(_ view: UIView) {
        super.addSubview(view)

        // Always bringing our overlay view on top
        guard let overlayView = overlayView else { return }

        bringSubview(toFront: overlayView)
    }

    override func sendEvent(_ event: UIEvent) {
        super.sendEvent(event)

        // Handle only touch events
        guard
            event.type == .touches,
            let touch = event.allTouches?.first,
            let overlayView = overlayView
        else { return }

        // Update our overlay view state
        let location = touch.location(in: nil)

        switch touch.phase {
        case .began, .moved:
            overlayView.setEmitting(true, in: location)
        case .cancelled, .ended:
            overlayView.setEmitting(false, in: location)
        case .stationary:
            // No-op
            break
        }
    }

    // MARK: - Private methods

    private func commonInit() {
        let overlayView = OverlayLogoEmittingView(frame: bounds)

        addSubview(overlayView)
        self.overlayView = overlayView
    }
}
