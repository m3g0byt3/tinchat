//
//  RoundedButton.swift
//  TinChat
//
//  Created by m3g0byt3 on 26/09/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
final class RoundedButton: UIButton {

    // MARK: - Public Properties

    @IBInspectable
    var cornerRadius: CGFloat = 0.0 {
        didSet {
            updateAppearance()
        }
    }

    @IBInspectable
    var cornerRadiusToWidthRatio: CGFloat = 0.0 {
        didSet {
            updateAppearance()
        }
    }

    @IBInspectable
    var cornerRadiusToHeightRatio: CGFloat = 0.0 {
        didSet {
            updateAppearance()
        }
    }

    @IBInspectable
    var borderWidth: CGFloat = 0.0 {
        didSet {
            updateAppearance()
        }
    }

    @IBInspectable
    var borderColor: UIColor = .black {
        didSet {
            updateAppearance()
        }
    }

    @IBInspectable
    var disabledBorderColor: UIColor = .black {
        didSet {
            updateAppearance()
        }
    }

    override var isEnabled: Bool {
        get {
            return super.isEnabled
        }
        set {
            updateAppearance()
            super.isEnabled = newValue
        }
    }

    // MARK: - Public API

    override func layoutSubviews() {
        super.layoutSubviews()
        updateAppearance()
    }

    // MARK: - Private API

    private func updateAppearance() {
        let cornerRadiusFromWidth = frame.width * cornerRadiusToWidthRatio
        let cornerRadiusFromHeight = frame.height * cornerRadiusToHeightRatio

        layer.cornerRadius = max(cornerRadius, cornerRadiusFromWidth, cornerRadiusFromHeight)
        layer.borderColor = isEnabled ? borderColor.cgColor : disabledBorderColor.cgColor
        layer.borderWidth = borderWidth
    }
}
