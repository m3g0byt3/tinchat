//
//  OverlayLogoEmittingView.swift
//  TinChat
//
//  Created by m3g0byt3 on 02/12/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

/// A `UIView` subclass that will emit particles using `CAEmitterLayer`.
final class OverlayLogoEmittingView: UIView {

    // MARK: - Public properties

    override class var layerClass: AnyClass {
        return CAEmitterLayer.self
    }

    // MARK: - Private properties

    private var emitterLayer: CAEmitterLayer? {
        return layer as? CAEmitterLayer
    }

    // MARK: - Initialization/Deinitialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    // MARK: - Public methods

    func setEmitting(_ isEmitting: Bool, in point: CGPoint) {
        emitterLayer?.isEmitting = isEmitting
        emitterLayer?.emitterPosition = point
    }

    // MARK: - Private methods

    private func commonInit() {
        let emitterCell = CAEmitterCell()

        emitterCell.birthRate = Constants.OverlayLogoEmittingView.birthRate
        emitterCell.lifetime = Constants.OverlayLogoEmittingView.lifetime
        emitterCell.velocityRange = Constants.OverlayLogoEmittingView.velocityRange
        emitterCell.scale = Constants.OverlayLogoEmittingView.scale
        emitterCell.spinRange = Constants.OverlayLogoEmittingView.spinRange
        emitterCell.alphaSpeed = Constants.OverlayLogoEmittingView.alphaSpeedConstant / emitterCell.lifetime
        emitterCell.emissionRange = Constants.OverlayLogoEmittingView.emissionRange
        emitterCell.contents = R.image.tinkoffLogo()?.cgImage

        emitterLayer?.emitterCells = [emitterCell]
        emitterLayer?.isEmitting = false

        isUserInteractionEnabled = false
    }
}
