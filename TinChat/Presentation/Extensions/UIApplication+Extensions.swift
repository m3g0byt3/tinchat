//  UIApplication+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 30/09/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    /// Open settings for current application (using `UIApplicationOpenSettingsURLString`).
    func openSettings() {
        guard let url = URL(string: UIApplicationOpenSettingsURLString) else { return }
        open(url)
    }
}
