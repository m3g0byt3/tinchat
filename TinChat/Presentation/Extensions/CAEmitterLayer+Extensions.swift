//
//  CAEmitterLayer+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 02/12/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

extension CAEmitterLayer {
    var isEmitting: Bool {
        get {
            return lifetime > 0
        }
        set {
            self.lifetime = newValue ? Constants.Interface.emitterLayerLifetime : 0
        }
    }
}
