//
//  UITextField+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 03/12/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    var isEmpty: Bool {
        return text?.isEmpty ?? true
    }
}
