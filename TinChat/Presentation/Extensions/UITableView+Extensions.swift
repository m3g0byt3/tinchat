//
//  UITableView+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 29/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func scrollToBottom() {
        if numberOfRows(inSection: 0) > 0 {
            let row = numberOfRows(inSection: 0) - 1
            let indexPath = IndexPath(row: row, section: 0)
            scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
}
