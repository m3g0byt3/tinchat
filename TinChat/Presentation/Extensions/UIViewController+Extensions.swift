//
//  UIViewController+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 30/09/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    typealias Action = (String, UIAlertAction.Closure)
}

extension UIAlertAction {
    typealias Closure = (UIAlertAction) -> Void
}

extension UIViewController {
    /// Presents alert with given title and message.
    /// - Parameters:
    ///     - title: Alert title.
    ///     - message: Alert message.
    ///     - actions: Array of available actions for this alert.
    func presentAlert(title: String, message: String, actions: [UIAlertController.Action] = []) {
        guard presentedViewController == nil else { return }

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: R.string.localizable.errorCancelAction(), style: .default)
        let alertActions = actions.map { UIAlertAction(title: $0.0, style: .default, handler: $0.1) }

        Array([alertActions, [closeAction]].joined()).forEach(alert.addAction)

        present(alert, animated: true)
    }

    func presentAlert(for error: Error, actions: [UIAlertController.Action] = []) {
        guard presentedViewController == nil else { return }

        let alert = UIAlertController(title: R.string.localizable.errorTitle(),
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        let closeAction = UIAlertAction(title: R.string.localizable.errorCancelAction(), style: .default)
        let alertActions = actions.map { UIAlertAction(title: $0.0, style: .default, handler: $0.1) }

        Array([[closeAction], alertActions].joined()).forEach(alert.addAction)

        present(alert, animated: true)
    }
}
