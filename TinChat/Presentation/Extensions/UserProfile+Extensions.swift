//
//  UserProfile+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 22/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

extension UserProfile {
    var image: UIImage? {
        return imageBlob.flatMap(UIImage.init)
    }

    func diff(to other: UserProfile) -> UserProfile {
        return UserProfile(username: username != other.username ? other.username : nil,
                           description: description != other.description ? other.description : nil,
                           imageBlob: imageBlob != other.imageBlob ? other.imageBlob : nil)
    }
}
