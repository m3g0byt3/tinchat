//
//  UIImagePickerController+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 30/09/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

extension UIImagePickerController {
    /// Array of currently available Source Types.
    static var availableSourceTypes: [UIImagePickerControllerSourceType] {
        // TODO: Replace hard-coded list of available source types with `.allCases` property
        // from `CaseIterable` protocol (Swift >= 4.2)
        return [.camera, .photoLibrary, .savedPhotosAlbum].filter(UIImagePickerController.isSourceTypeAvailable)
    }
}

extension UIImagePickerControllerSourceType: CustomStringConvertible {
    public var description: String {
        switch self {
        case .camera: return R.string.localizable.uiImagePickerControllerSourceTypeCamera()
        case .photoLibrary: return R.string.localizable.uiImagePickerControllerSourceTypePhotoLibrary()
        case .savedPhotosAlbum: return R.string.localizable.uiImagePickerControllerSourceTypeSavedPhotosAlbum()
        }
    }
}
