//
//  PresentationLayerConstants.swift
//  TinChat
//
//  Created by m3g0byt3 on 25/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

extension Constants {
    enum Interface {
        static let numberOfPhotosInColumn = 3
        static let toolbarHeightRatio: CGFloat = 1.5
        static let emitterLayerLifetime: Float = 5.0

        static let titleAnimationDuration: TimeInterval = 1.0
        static let titleAnimationType = kCATransitionFade
        static let titleKeyPath = #keyPath(UINavigationBar.titleTextAttributes)

        static let buttonAnimationDuration: TimeInterval = 0.5
        static let buttonInsets = UIEdgeInsets(top: 0, left: 12.0, bottom: 0, right: -12.0)
        static let buttonTransform = CGAffineTransform(scaleX: 1.15, y: 1.15)

        /// Default `UINavigationBar` title size
        static let offlineTitleSize: CGFloat = 17.0

        /// ~10% larger than default size
        static var onlineTitleSize: CGFloat {
            return (offlineTitleSize * 1.1).rounded(.up)
        }

        static var onlineTitleAttributes: [NSAttributedStringKey: Any] {
            return [
                .font: UIFont.systemFont(ofSize: Constants.Interface.onlineTitleSize, weight: .semibold),
                .foregroundColor: R.clr.tinchat.onlineTitle()
            ]
        }

        static var offlineTitleAttributes: [NSAttributedStringKey: Any] {
            return [
                .font: UIFont.systemFont(ofSize: Constants.Interface.offlineTitleSize, weight: .semibold),
                .foregroundColor: R.clr.tinchat.offlineTitle()
            ]
        }
    }

    enum OverlayLogoEmittingView {
        static let birthRate: Float = 10.0
        static let lifetime: Float = 5.0
        static let velocityRange: CGFloat = 500.0
        static let scale: CGFloat = 1.0 / 3.0
        static let spinRange: CGFloat = 10.0
        static let alphaSpeedConstant: Float = -1.0
        static let emissionRange = CGFloat.pi * 2.0
    }
}
