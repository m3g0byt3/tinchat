//
//  MessageViewModel.swift
//  TinChat
//
//  Created by m3g0byt3 on 06/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

// TODO: Add  human-readable message date
struct MessageViewModel {
    let text: String
}

// MARK: - Convenience initializer from model

extension MessageViewModel {
    init?(model: MessageMO) {
        guard let text = model.text else { return nil }
        self.text = text
    }
}
