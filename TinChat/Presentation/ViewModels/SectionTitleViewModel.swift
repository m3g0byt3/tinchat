//
//  SectionTitleViewModel.swift
//  TinChat
//
//  Created by m3g0byt3 on 11/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

struct SectionTitleViewModel {
    let title: String

    init(rawTitle: String) {
        self.title = rawTitle == "1"
            ? R.string.localizable.onlineSection()
            : R.string.localizable.offlineSection()
    }
}
