//
//  ConversationViewModel.swift
//  TinChat
//
//  Created by m3g0byt3 on 05/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

struct ConversationViewModel {

    // MARK: - Private properties

    private static let formatter = DateFormatter()

    // MARK: - Public properties

    let name: String
    let date: String
    let message: String
    let isOnline: Bool
    let hasUnreadMessages: Bool

    // MARK: - Private API

    private static func prettyPrinted(_ date: Date?) -> String {
        guard let date = date else { return "" }
        let isTodayDate = Calendar.current.isDateInToday(date)

        formatter.dateStyle = isTodayDate ? .none : .short
        formatter.timeStyle = isTodayDate ? .short : .none

        return formatter.string(from: date)
    }
}

// MARK: - Convenience initializer from model

extension ConversationViewModel {
    init?(model: ConversationMO) {
        guard let name = model.user?.name else { return nil }

        self.name = name
        self.date = ConversationViewModel.prettyPrinted(model.messagesSorted.last?.date)
        self.message = model.messagesSorted.last?.text ?? R.string.localizable.noMessages()
        self.isOnline = model.user?.isOnline ?? false
        self.hasUnreadMessages = model.hasUnreadMessages
    }
}
