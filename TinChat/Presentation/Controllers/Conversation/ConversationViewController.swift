//
//  ConversationViewController.swift
//  TinChat
//
//  Created by m3g0byt3 on 06/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit
import CoreData

final class ConversationViewController: BaseViewController {

    // MARK: - IBOutlets/UI

    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var sendBarButton: UIBarButtonItem!
    @IBOutlet private weak var toolbar: UIToolbar!
    @IBOutlet private weak var toolbarConstraint: NSLayoutConstraint!

    // MARK: - Public Properties

    // swiftlint:disable implicitly_unwrapped_optional
    var conversationService: ConversationServiceProtocol!
    var fetchedResultsController: NSFetchedResultsController<MessageMO>!
    // swiftlint:enable implicitly_unwrapped_optional

    // MARK: - Private Properties

    private var conversationIdentifier: String?
    private var cachedStatus: Peer.Status = .offline
    private var cachedIsTextFieldEmpty: Bool = true

    private var sendButton: UIButton? {
        return sendBarButton.customView as? UIButton
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupServices()
        updateTitleAppearanceForStatus(cachedStatus)
        updateButtonAppearanceForStatus(cachedStatus)
        fetchData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: toolbar.frame.height, right: 0)
        textField.frame.size.width = view.frame.width - toolbar.frame.height * Constants.Interface.toolbarHeightRatio
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // TODO: Better scroll to bottom implementation
        tableView.scrollToBottom()
    }

    // MARK: - Overrides

    override func didChangeKeyboardFrameHeight(_ height: CGFloat) {
        let bottomInset = height + toolbar.frame.height

        toolbarConstraint.constant = height
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: bottomInset, right: 0)
        tableView.scrollIndicatorInsets = tableView.contentInset
        tableView.scrollToBottom()
    }

    override func controllerDidChangeContent(
        _ controller: NSFetchedResultsController<NSFetchRequestResult>
    ) {
        super.controllerDidChangeContent(controller)
        tableView.scrollToBottom()
    }

    // MARK: - Private API

    private func setupUI() {
        let sendButton = UIButton(type: .system)

        sendButton.setImage(R.image.sendIcon(), for: .normal)
        sendButton.addTarget(self, action: #selector(sendButtonHandler), for: .touchUpInside)
        sendButton.imageEdgeInsets = Constants.Interface.buttonInsets

        sendBarButton.customView = sendButton
        tableView.tableFooterView = UIView()
        tableView.register(R.nib.inboundMessageCell)
        tableView.register(R.nib.outboundMessageCell)
    }

    private func setupServices() {
        conversationService.delegate = self
        fetchedResultsController.delegate = self
    }

    private func fetchData() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            presentAlert(for: error)
        }
    }

    // TODO: Add `animated: Bool` option (if needed)
    private func updateTitleAppearanceForStatus(_ status: Peer.Status) {
        let animation = CATransition()
        let attributes = status == .online
            ? Constants.Interface.onlineTitleAttributes
            : Constants.Interface.offlineTitleAttributes

        animation.type = Constants.Interface.titleAnimationType
        animation.duration = Constants.Interface.titleAnimationDuration
        navigationController?.navigationBar.layer.add(animation, forKey: Constants.Interface.titleKeyPath)
        navigationController?.navigationBar.titleTextAttributes = attributes

        // Cache new value
        cachedStatus = status
    }

    // TODO: Add `animated: Bool` option (if needed)
    private func updateButtonAppearanceForStatus(_ status: Peer.Status) {
        // Early exit if both `textField.isEmpty` property and cached status haven't changed since last call
        guard cachedIsTextFieldEmpty != textField.isEmpty || cachedStatus != status else { return }

        let isSendButtonEnabled = status == .online && !textField.isEmpty

        UIView.animate(withDuration: Constants.Interface.buttonAnimationDuration) {
            self.sendBarButton.isEnabled = isSendButtonEnabled
            self.sendButton?.transform = Constants.Interface.buttonTransform
        }

        UIView.animate(
            withDuration: Constants.Interface.buttonAnimationDuration,
            delay: Constants.Interface.buttonAnimationDuration,
            animations: { self.sendButton?.transform = .identity },
            completion: nil
        )

        // Cache new value
        cachedIsTextFieldEmpty = textField.isEmpty
    }

    // MARK: - IBActions/Control handlers

    @IBAction private func sendButtonHandler() {
        guard
            let identifier = conversationIdentifier,
            let text = textField.text,
            !text.isEmpty
        else { return }
        let message = Message(text: text)

        conversationService.send(message, inConversationWithIdentifier: identifier)
        textField.text = ""
        updateButtonAppearanceForStatus(cachedStatus)
    }

    @IBAction private func textFieldDidEndEditing(_ sender: UITextField) {
        sender.resignFirstResponder()
    }

    @IBAction private func textFieldDidChangeText(_ sender: UITextField) {
        updateButtonAppearanceForStatus(cachedStatus)
    }
}

// MARK: - UITableViewDataSource protocol conformance

extension ConversationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = fetchedResultsController.object(at: indexPath)

        let identifier = model.direction == .inbound
            ? R.reuseIdentifier.inboundMessageCell
            : R.reuseIdentifier.outboundMessageCell

        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) else {
            assertionFailure("Unable to dequeue cell for identifier \(identifier.identifier).")
            return UITableViewCell()
        }

        if let viewModel = MessageViewModel(model: model) {
            cell.configure(with: viewModel)
        }

        return cell
    }
}

// MARK: - Configurable protocol conformance

extension ConversationViewController: Configurable {
    typealias Model = ConversationMO

    @discardableResult
    func configure(with model: Model) -> Self {
        guard
            let identifier = model.id,
            let isOnline = model.user?.isOnline
        else { return self }
        let request = NSFetchRequest.messagesFromConversation(withIdentifier: identifier)

        fetchedResultsController.fetchRequest.predicate = request.predicate
        navigationItem.title = model.user?.name
        conversationIdentifier = model.id
        cachedStatus = isOnline ? .online : .offline

        return self
    }
}

// MARK: - ConversationServiceDelegate protocol conformance

extension ConversationViewController: ConversationServiceDelegate {
    func conversationService(
        _ service: ConversationServiceProtocol,
        didDetectStatusChange status: Peer.Status,
        forPeer peer: Peer
    ) {
        updateButtonAppearanceForStatus(status)
        updateTitleAppearanceForStatus(status)
    }

    func conversationService(
        _ service: ConversationServiceProtocol,
        didFailWithError error: Error
    ) {
        presentAlert(for: error)
    }
}
