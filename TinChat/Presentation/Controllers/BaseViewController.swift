//
//  BaseViewController.swift
//  TinChat
//
//  Created by m3g0byt3 on 29/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class BaseViewController: UIViewController {

    // MARK: - IBOutlets/UI

    // swiftlint:disable:next private_outlet
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Initialization/Deinitialization

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        // Avoid initialization of abstract class
        guard type(of: self) != BaseViewController.self else {
            fatalError("Create a subclass instance of abstract class \(BaseViewController.self).")
        }
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        // Avoid initialization of abstract class
        guard type(of: self) != BaseViewController.self else {
            fatalError("Create a subclass instance of abstract class \(BaseViewController.self).")
        }
        super.init(coder: aDecoder)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupObservers()
    }

    // MARK: - Public API

    func didChangeKeyboardFrameHeight(_ height: CGFloat) {}

    // MARK: - Private API

    private func setupObservers() {
        let center = NotificationCenter.default
        let willShow = #selector(keyboardShown(_:))
        let willHide = #selector(keyboardDismissed(_:))
        center.addObserver(self, selector: willShow, name: .UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: willHide, name: .UIKeyboardWillHide, object: nil)
    }

    @objc private func keyboardShown(_ notification: Notification) {
        guard let parsed = KeyboardNotification(notification) else { return }
        didChangeKeyboardFrameHeight(parsed.endFrame.height)
    }

    @objc private func keyboardDismissed(_ notification: Notification) {
        didChangeKeyboardFrameHeight(0)
    }
}

// MARK: - NSFetchedResultsControllerDelegate protocol conformance

extension BaseViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    func controller(
        _ controller: NSFetchedResultsController<NSFetchRequestResult>,
        didChange sectionInfo: NSFetchedResultsSectionInfo,
        atSectionIndex sectionIndex: Int,
        for type: NSFetchedResultsChangeType
    ) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .update:
            tableView.reloadSections(IndexSet(integer: sectionIndex), with: .automatic)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .automatic)
        default:
            assertionFailure()
        }
    }

    func controller(
        _ controller: NSFetchedResultsController<NSFetchRequestResult>,
        didChange anObject: Any,
        at indexPath: IndexPath?,
        for type: NSFetchedResultsChangeType,
        newIndexPath: IndexPath?
    ) {
        switch type {
        case .delete:
            guard let indexPath = indexPath else { return }
            tableView.deleteRows(at: [indexPath], with: .automatic)
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            tableView.moveRow(at: indexPath, to: newIndexPath)
        case .update:
            guard let indexPath = indexPath else { return }
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }

    func controller(
        _ controller: NSFetchedResultsController<NSFetchRequestResult>,
        sectionIndexTitleForSectionName sectionName: String
    ) -> String? {
        let viewModel = SectionTitleViewModel(rawTitle: sectionName)

        return viewModel.title
    }
}
