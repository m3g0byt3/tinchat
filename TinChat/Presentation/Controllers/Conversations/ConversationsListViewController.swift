//
//  ConversationListViewController.swift
//  TinChat
//
//  Created by m3g0byt3 on 03/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit
import CoreData

final class ConversationsListViewController: BaseViewController {

    // MARK: - Public Properties

    // swiftlint:disable implicitly_unwrapped_optional
    var conversationService: ConversationServiceProtocol!
    var fetchedResultsController: NSFetchedResultsController<ConversationMO>!
    // swiftlint:enable implicitly_unwrapped_optional

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupServices()
        fetchData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // We have to re-assign itself as a delegate for `conversationService` because this property
        // may be overwritten by presented `ConversationViewController` instance
        conversationService.delegate = self
    }

    // MARK: - Private API

    private func setupUI() {
        tableView.tableFooterView = UIView()
        tableView.register(R.nib.conversationsListCell)
        guard #available(iOS 11, *) else {
            automaticallyAdjustsScrollViewInsets = false
            return
        }
    }

    private func setupServices() {
        fetchedResultsController.delegate = self
        conversationService.isOnline = true
    }

    private func fetchData() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            presentAlert(for: error)
        }
    }

    // MARK: - IBActions/Control handlers

    @IBAction private func showProfileButtonHandler(_ sender: UIBarButtonItem) {
        if let profileView = R.storyboard.profileViewController.instantiateInitialViewController() {
            navigationController?.pushViewController(profileView, animated: true)
        }
    }
}

// MARK: - UITableViewDataSource protocol conformance

extension ConversationsListViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = fetchedResultsController.object(at: indexPath)
        let identifier = R.reuseIdentifier.conversationsListCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) else {
            assertionFailure("Unable to dequeue cell for identifier \(identifier.identifier).")
            return UITableViewCell()
        }

        if let viewModel = ConversationViewModel(model: model) {
            cell.configure(with: viewModel)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionInfo = fetchedResultsController.sections?[section]
        return sectionInfo?.indexTitle
    }
}

// MARK: - UITableViewDelegate protocol conformance

extension ConversationsListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if let conversationView = R.storyboard.conversationViewController.instantiateInitialViewController() {
            let model = fetchedResultsController.object(at: indexPath)

            conversationView.configure(with: model)
            conversationView.conversationService = conversationService
            navigationController?.pushViewController(conversationView, animated: true)
        }
    }
}

// MARK: - ConversationServiceDelegate protocol conformance

extension ConversationsListViewController: ConversationServiceDelegate {
    func conversationService(
        _ service: ConversationServiceProtocol,
        didDetectStatusChange status: Peer.Status,
        forPeer peer: Peer
    ) {
        // No-op since we don't have to handle status changes for concrete peer
    }

    func conversationService(
        _ service: ConversationServiceProtocol,
        didFailWithError error: Error
    ) {
        presentAlert(for: error)
    }
}
