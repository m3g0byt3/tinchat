//
//  ProfileImageDownloadController.swift
//  TinChat
//
//  Created by m3g0byt3 on 25/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

final class ProfileImageDownloadController: UIViewController {

    // MARK: - IBOutlets/UI

    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var flowLayout: UICollectionViewFlowLayout!

    // MARK: - Public properties

    var userDidPickImage: ((UIImage?) -> Void)?
    // swiftlint:disable:next implicitly_unwrapped_optional
    var photoService: PhotoServiceProtocol!

    // MARK: - Private properties

    private var photos =  [Photo]()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        fetchInitialData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureFlowLayout()
    }

    // MARK: - Private methods

    private func setupUI() {
        collectionView.register(R.nib.photoCell)
    }

    private func updateUI() {
        self.collectionView.reloadData()
    }

    private func configureFlowLayout() {
        let cellWidth = view.frame.width / CGFloat(Constants.Interface.numberOfPhotosInColumn)
        let cellHeight = cellWidth

        flowLayout.itemSize = CGSize(width: cellWidth, height: cellHeight)
    }

    private func fetchInitialData() {
        activityIndicator.startAnimating()
        photoService.getPhotos { [weak self] result in
            self?.activityIndicator.stopAnimating()
            switch result {
            case .success(let model):
                self?.photos = model
                self?.updateUI()
            case .failure(let error):
                self?.presentAlert(for: error)
            }
        }
    }

    // MARK: - IBActions/Control handlers

    @IBAction private func closeButtonHandler(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
}

// MARK: - UINavigationBarDelegate protocol conformance

extension ProfileImageDownloadController: UINavigationBarDelegate {
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}

// MARK: - UICollectionViewDataSource protocol conformance

extension ProfileImageDownloadController: UICollectionViewDataSource {
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        return photos.count
    }

    public func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        let identifier = R.reuseIdentifier.photoCell
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) else {
            assertionFailure("Unable to dequeue cell for identifier \(identifier.identifier).")
            return UICollectionViewCell()
        }

        return cell
    }
}

// MARK: - UICollectionViewDelegate protocol conformance

extension ProfileImageDownloadController: UICollectionViewDelegate {
    func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        let photo = photos[indexPath.row]

        collectionView.deselectItem(at: indexPath, animated: true)
        photoService.downloadPhoto(photo) { [weak self] result in
            switch result {
            case .success(let model):
                self?.userDidPickImage?(model.image)
            case .failure(let error):
                self?.presentAlert(for: error)
            }
        }
    }

    func collectionView(
        _ collectionView: UICollectionView,
        willDisplay cell: UICollectionViewCell,
        forItemAt indexPath: IndexPath
    ) {
        guard let cell = cell as? PhotoCell else { return }
        let photo = photos[indexPath.row]

        photoService.downloadPhoto(photo) { [weak cell = cell] result in
            switch result {
            case .success(let model):
                cell?.configure(with: model)
            case .failure:
                // No-op because individual image loading failure is not critical for us
                break
            }
        }
    }

    func collectionView(
        _ collectionView: UICollectionView,
        didEndDisplaying cell: UICollectionViewCell,
        forItemAt indexPath: IndexPath
    ) {
        let photo = photos[indexPath.row]

        photoService.cancelDownloadingPhoto(photo)
    }
}
