//
//  ProfileViewController.swift
//  TinChat
//
//  Created by m3g0byt3 on 23/09/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

final class ProfileViewController: UIViewController {

    // MARK: - IBOutlets/UI

    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var editButton: RoundedButton!

    // MARK: - Public properties

    // swiftlint:disable:next implicitly_unwrapped_optional
    var dataManager: DataManagerProtocol!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }

    // MARK: - IBActions/Control handlers

    @IBAction private func editButtonHandler(_ sender: UIBarButtonItem) {
        if let profileEditView = R.storyboard.profileEditViewController.instantiateInitialViewController() {
            present(profileEditView, animated: true)
        }
    }

    // MARK: - Private API

    private func setupUI() {
        usernameLabel.font = UIFont.preferredFont(forTextStyle: .title1, withSymbolicTraits: .traitBold)
    }

    private func loadData() {
        dataManager.loadUserProfile { [weak self] result in
            switch result {
            case .success(let model): self?.configure(with: model)
            case .failure(let error): self?.presentAlert(for: error)
            }
        }
    }
}

// MARK: - Configurable protocol conformance

extension ProfileViewController: Configurable {
    typealias Model = UserProfile

    @discardableResult
    func configure(with model: Model) -> Self {
        model.image.flatMap { avatarImageView.image = $0 }
        model.username.flatMap { usernameLabel.text = $0 }
        model.description.flatMap { descriptionLabel.text = $0 }

        return self
    }
}
