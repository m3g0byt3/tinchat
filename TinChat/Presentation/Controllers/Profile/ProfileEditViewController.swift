//
//  ProfileEditViewController.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

final class ProfileEditViewController: BaseViewController {

    // MARK: - IBOutlets/UI

    @IBOutlet private weak var avatarSelectionButton: RoundedButton!
    @IBOutlet private weak var saveButton: RoundedButton!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var descriptionTextView: UITextView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var scrollView: UIScrollView!

    // MARK: - Public properties

    // swiftlint:disable:next implicitly_unwrapped_optional
    var dataManager: DataManagerProtocol!

    // MARK: - Private properties

    private var cachedUserProfile: UserProfile?
    private var changedUserProfile: UserProfile?

    // MARK: - Initialization/Deinitialization

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupObservers()
        setupUI()
        loadInitialData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        avatarImageView.layer.cornerRadius = avatarSelectionButton.layer.cornerRadius
    }

    // MARK: - Overrides

    override func didChangeKeyboardFrameHeight(_ height: CGFloat) {
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: height, right: 0)
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }

    // MARK: - Private API

    // MARK: UI stuff

    private func setupUI() {
        saveButton.setTitleColor(UIColor.lightGray, for: .disabled)
    }

    private func updateUI() {
        let isSavingAllowed = cachedUserProfile != changedUserProfile && !activityIndicator.isAnimating
        saveButton.isEnabled = isSavingAllowed
    }

    // MARK: Loading/Saving data

    private func loadInitialData() {
        self.cachedUserProfile = UserProfile()
        dataManager.loadUserProfile { [weak self] result in
            switch result {
            case .success(let model): self?.configure(with: model)
            case .failure(let error): self?.presentAlert(for: error)
            }
        }
    }

    private func saveData() {
        UIResponder.current?.resignFirstResponder()
        guard let cached = cachedUserProfile, let changed = changedUserProfile else { return }
        let diff = cached.diff(to: changed)
        activityIndicator.startAnimating()
        updateUI()
        dataManager.saveUserProfile(diff) { [weak self] result in
            self?.activityIndicator.stopAnimating()
            switch result {
            case .success:
                self?.cachedUserProfile = self?.changedUserProfile
                self?.presentAlert(title: R.string.localizable.successTitle(),
                                   message: R.string.localizable.completed())
            case .failure(let error):
                let retryClosure: UIAlertAction.Closure = { [weak self] _ in self?.saveData() }
                let retryAction = (R.string.localizable.retry(), retryClosure)
                self?.presentAlert(for: error, actions: [retryAction])
                self?.updateUI()
            }
        }
    }

    @objc private func updateModel() {
        // Prevents model update using default placeholder image
        let avatarImage = avatarImageView.image != R.image.imagePlaceholder() ? avatarImageView.image : nil
        changedUserProfile = UserProfile(username: usernameTextField.text,
                                         description: descriptionTextView.text,
                                         imageBlob: avatarImage.flatMap(UIImagePNGRepresentation))
        updateUI()
    }

    private func setupObservers() {
        let center = NotificationCenter.default
        let selector = #selector(updateModel)
        center.addObserver(self, selector: selector, name: .UITextFieldTextDidChange, object: nil)
        center.addObserver(self, selector: selector, name: .UITextViewTextDidChange, object: nil)
    }

    // MARK: UIImagePickerController stuff

    private func alertFactory() -> UIAlertController {
        let selectionActions = UIImagePickerController.availableSourceTypes.map(alertAction)
        let cancelAction = UIAlertAction(title: R.string.localizable.actionSheetCancelAction(), style: .cancel)
        let downloadAction = UIAlertAction(
            title: R.string.localizable.actionSheetDownloadAction(),
            style: .default,
            handler: { [weak self] _ in self?.showProfileImageDownloadController() }
        )
        let controller = UIAlertController(title: R.string.localizable.actionSheetTitle(),
                                           message: nil,
                                           preferredStyle: .actionSheet)

        Array([selectionActions, [downloadAction], [cancelAction]].joined()).forEach(controller.addAction)

        return controller
    }

    private func alertAction(for sourceType: UIImagePickerControllerSourceType) -> UIAlertAction {
        return UIAlertAction(title: String(describing: sourceType), style: .default) { [weak self] _ in
            if sourceType == .camera && AVCaptureDevice.authorizationStatus(for: .video) != .authorized {
                AVCaptureDevice.requestAccess(for: .video) { isAllowed in
                    if isAllowed {
                        self?.showImagePicker(for: sourceType)
                    } else {
                        self?.showCameraAccessErrorAlert()
                    }
                }
            } else {
                self?.showImagePicker(for: sourceType)
            }
        }
    }

    private func showCameraAccessErrorAlert() {
        let errorAlertClosure: UIAlertAction.Closure = { _ in UIApplication.shared.openSettings() }
        let errorAlertAction = (R.string.localizable.noCameraAccessAction(), errorAlertClosure)

        presentAlert(title: R.string.localizable.errorTitle(),
                     message: R.string.localizable.noCameraAccessErrorMessage(),
                     actions: [errorAlertAction])
    }

    private func showImagePicker(for sourceType: UIImagePickerControllerSourceType) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        imagePickerController.sourceType = sourceType
        present(imagePickerController, animated: true)
    }

    private func showProfileImageDownloadController() {
        guard let viewController = R.storyboard.profileImageDownloadController.instantiateInitialViewController() else {
            return
        }

        viewController.userDidPickImage = { [weak self] image in
            guard let `self` = self else { return }

            self.avatarImageView.image = image
            self.updateModel()
            self.dismiss(animated: true)
        }

        present(viewController, animated: true)
    }

    // MARK: - IBActions/Control handlers

    @IBAction private func closeButtonHandler(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }

    @IBAction private func photoSelectionButtonHandler(_ sender: UIButton) {
        present(alertFactory(), animated: true)
    }

    @IBAction private func saveButtonHandler(_ sender: UIButton) {
        saveData()
    }
}

// MARK: - UIImagePickerControllerDelegate & UINavigationControllerDelegate protocol conformance

extension ProfileEditViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        defer { picker.dismiss(animated: true) }
        guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else { return }

        avatarImageView.image = image
        updateModel()
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}

// MARK: - UINavigationBarDelegate protocol conformance

extension ProfileEditViewController: UINavigationBarDelegate {
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}

// MARK: - Configurable protocol conformance

extension ProfileEditViewController: Configurable {
    typealias Model = UserProfile

    @discardableResult
    func configure(with model: Model) -> Self {
        model.image.flatMap { avatarImageView.image = $0 }
        model.username.flatMap { usernameTextField.text = $0 }
        model.description.flatMap { descriptionTextView.text = $0 }
        cachedUserProfile = model
        return self
    }
}
