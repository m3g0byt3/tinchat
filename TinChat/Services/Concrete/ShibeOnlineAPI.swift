//
//  ShibeOnlineAPI.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// https://shibe.online API enpoints.
enum ShibeOnlineAPI {
    case getShibes
    case downloadPhoto(Photo)
}

// MARK: - APIProtocol protocol conformance

extension ShibeOnlineAPI: APIProtocol {
    var scheme: String {
        return "https"
    }

    var base: String {
        switch self {
        case .getShibes:
            return "shibe.online"
        case .downloadPhoto:
            return "cdn.shibe.online"
        }
    }

    var path: String {
        switch self {
        case .getShibes:
            return "/api/shibes"
        case .downloadPhoto(let photo):
            return "/shibes/" + photo.url.lastPathComponent
        }
    }

    var queryItems: [URLQueryItem] {
        switch self {
        case .getShibes:
            let count = URLQueryItem(name: "count", value: "\(Constants.Networking.count)")
            return [count]
        case .downloadPhoto:
            return []
        }
    }
}

// MARK: - URLRepresentable protocol conformance

extension ShibeOnlineAPI: URLRepresentable {}
