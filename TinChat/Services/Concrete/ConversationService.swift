//
//  ConversationService.swift
//  TinChat
//
//  Created by m3g0byt3 on 11/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Provides message sending, receiving and conversation/user management
/// using `CommunicationServiceProtocol` and `PersistenceServiceProtocol` core components.
final class ConversationService {

    // MARK: - Public Properties

    weak var delegate: ConversationServiceDelegate?

    var isOnline: Bool {
        didSet {
            communicationService.online = isOnline
        }
    }

    // MARK: - Private Properties

    private let communicationService: CommunicationServiceProtocol
    private let persistenceService: PersistenceServiceProtocol

    // MARK: - Initialization/Deinitialization

    init(
        communicationService: CommunicationServiceProtocol,
        persistenceService: PersistenceServiceProtocol,
        delegate: ConversationServiceDelegate? = nil
    ) {
        self.communicationService = communicationService
        self.persistenceService = persistenceService
        self.delegate = delegate
        self.isOnline = false
    }
}

// MARK: - ConversationService protocol conformance

extension ConversationService: ConversationServiceProtocol {
    func send(_ message: Message, inConversationWithIdentifier identifier: String) {
        guard let peer = persistenceService.peerForConversationWithIdentifier(identifier) else {
            return
        }

        persistenceService.append(message, toConversationWithIdentifier: identifier)
        communicationService.send(message, to: peer)
    }
}

// MARK: - CommunicationServiceDelegate protocol conformance

extension ConversationService: CommunicationServiceDelegate {
    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didFoundPeer peer: Peer
    ) {
        persistenceService.peer(peer, setStatus: .online)
        delegate?.conversationService(self, didDetectStatusChange: .online, forPeer: peer)
    }

    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didLostPeer peer: Peer
    ) {
        persistenceService.peer(peer, setStatus: .offline)
        delegate?.conversationService(self, didDetectStatusChange: .offline, forPeer: peer)
    }

    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didNotStartBrowsingForPeers error: Error
    ) {
        delegate?.conversationService(self, didFailWithError: error)
    }

    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didReceiveInviteFromPeer peer: Peer,
        invitationClosure: (Bool) -> Void
    ) {
        invitationClosure(true)
    }

    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didNotStartAdvertisingForPeers error: Error
    ) {
        delegate?.conversationService(self, didFailWithError: error)
    }

    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didReceiveMessage message: Message,
        from peer: Peer
    ) {
        guard let identifier = persistenceService.conversationIdentifierForPeer(peer) else {
            return
        }

        persistenceService.append(message, toConversationWithIdentifier: identifier)
    }
}
