//
//  ShibeOnlineService.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

// TODO: Add disk/in-memory cache
// TODO: Add Reachability connection check
/// Concrete implementation for `PhotoServiceProtocol` protocol using https://shibe.online API.
final class ShibeOnlineService {

    // MARK: - Private properties

    private let provider: AnyProvider<ShibeOnlineAPI>
    private let modelParser: AnyParser<[Photo]>
    private let photoParser: AnyParser<PhotoWrapper>
    private var tokens = [URL: NetworkToken]()

    // MARK: - Initialization/Deinitialization

    init<Provider: APIProviderProtocol, ModelParser: ParserProtocol, PhotoParser: ParserProtocol>(
        provider: Provider,
        modelParser: ModelParser,
        photoParser: PhotoParser
    ) where
        Provider.APIType == ShibeOnlineAPI,
        ModelParser.ModelType == [Photo],
        PhotoParser.ModelType == PhotoWrapper
    {
        // swiftlint:disable:previous opening_brace
        self.provider = AnyProvider(provider)
        self.modelParser = AnyParser(modelParser)
        self.photoParser = AnyParser(photoParser)
    }
}

// MARK: - PhotoServiceProtocol protocol conformance

extension ShibeOnlineService: PhotoServiceProtocol {
    func getPhotos(_ completion: @escaping (Result<[Photo], NetworkError>) -> Void) {
        provider.performRequest(.getShibes) { result in
            switch result {
            case .success(let data):
                guard let model = self.modelParser.parse(data) else {
                    completion(.failure(.mapping))
                    return
                }
                completion(.success(model))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func downloadPhoto(_ photo: Photo, completion: @escaping (Result<PhotoWrapper, NetworkError>) -> Void) {
        let token = provider.performRequest(.downloadPhoto(photo)) { result in
            self.tokens.removeValue(forKey: photo.url)

            switch result {
            case .success(let data):
                guard let model = self.photoParser.parse(data) else {
                    completion(.failure(.mapping))
                    return
                }
                completion(.success(model))
            case .failure(let error):
                completion(.failure(error))
            }
        }

        token.map { tokens[photo.url] = $0 }
    }

    func cancelDownloadingPhoto(_ photo: Photo) {
        tokens[photo.url]?.cancel()
    }
}
