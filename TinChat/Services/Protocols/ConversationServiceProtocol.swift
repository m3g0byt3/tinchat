//
//  ConversationServiceProtocol.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Provides message sending, receiving and conversation/user management.
protocol ConversationServiceProtocol {
    var delegate: ConversationServiceDelegate? { get set }
    var isOnline: Bool { get set }

    func send(_ message: Message, inConversationWithIdentifier identifier: String)
}
