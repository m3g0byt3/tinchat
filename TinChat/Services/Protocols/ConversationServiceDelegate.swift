//
//  ConversationServiceDelegate.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// `ConversationServiceProtocol` instance delegate.
protocol ConversationServiceDelegate: AnyObject {
    func conversationService(
        _ service: ConversationServiceProtocol,
        didDetectStatusChange status: Peer.Status,
        forPeer peer: Peer
    )

    func conversationService(
        _ service: ConversationServiceProtocol,
        didFailWithError error: Error
    )
}
