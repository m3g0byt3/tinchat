//
//  PhotoServiceProtocol.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Provides data fetching and image downloading
protocol PhotoServiceProtocol {
    func getPhotos(_ completion: @escaping (Result<[Photo], NetworkError>) -> Void)
    func downloadPhoto(_ photo: Photo, completion: @escaping (Result<PhotoWrapper, NetworkError>) -> Void)
    func cancelDownloadingPhoto(_ photo: Photo)
}
