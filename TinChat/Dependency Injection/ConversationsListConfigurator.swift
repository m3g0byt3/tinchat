//
//  ConversationsListConfigurator.swift
//  TinChat
//
//  Created by m3g0byt3 on 06/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import CoreData

final class ConversationsListConfigurator: NSObject {

    // MARK: - IBOutlets and UI

    @IBOutlet private weak var conversationsListViewController: ConversationsListViewController!

    // MARK: - Public API

    override func awakeFromNib() {
        super.awakeFromNib()
        // TODO: Resolve dependencies using DI Assembly
        let communicationService = MCCommunicationService()
        let persistenceService = CoreDataPersistenceService()
        let conversationService = ConversationService(
            communicationService: communicationService,
            persistenceService: persistenceService
        )

        communicationService.delegate = conversationService
        conversationService.delegate = conversationsListViewController
        conversationsListViewController.conversationService = conversationService

        let request: NSFetchRequest<ConversationMO> = ConversationMO.fetchRequest()
        let onlineDescriptor = NSSortDescriptor(key: #keyPath(ConversationMO.user.isOnline), ascending: true)
        let unreadDescriptor = NSSortDescriptor(key: #keyPath(ConversationMO.hasUnreadMessages), ascending: true)

        request.sortDescriptors = [onlineDescriptor, unreadDescriptor]

        let frc = NSFetchedResultsController(fetchRequest: request,
                                             managedObjectContext: CoreDataStack.shared.mainContext,
                                             sectionNameKeyPath: #keyPath(ConversationMO.user.isOnline),
                                             cacheName: nil)

        conversationsListViewController.fetchedResultsController = frc
    }
}
