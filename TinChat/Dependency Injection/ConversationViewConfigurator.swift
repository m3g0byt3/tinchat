//
//  ConversationViewConfigurator.swift
//  TinChat
//
//  Created by m3g0byt3 on 12/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import CoreData

final class ConversationViewConfigurator: NSObject {

    // MARK: - IBOutlets and UI

    @IBOutlet private weak var conversationViewController: ConversationViewController!

    // MARK: - Public API

    override func awakeFromNib() {
        super.awakeFromNib()
        // TODO: Resolve depеndencies using DI Assembly
        let request: NSFetchRequest<MessageMO> = MessageMO.fetchRequest()
        let dateDescriptor = NSSortDescriptor(key: #keyPath(MessageMO.date), ascending: true)

        request.sortDescriptors = [dateDescriptor]

        let frc = NSFetchedResultsController(fetchRequest: request,
                                             managedObjectContext: CoreDataStack.shared.mainContext,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil)

        conversationViewController.fetchedResultsController = frc
    }
}
