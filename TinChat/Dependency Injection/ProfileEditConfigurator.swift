//
//  ProfileEditConfigurator.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

final class ProfileEditConfigurator: NSObject {

    // MARK: - IBOutlets and UI

    @IBOutlet private weak var profileEditViewController: ProfileEditViewController!

    // MARK: - Public API

    override func awakeFromNib() {
        super.awakeFromNib()
        // TODO: Resolve depеndencies using DI Assembly
        profileEditViewController.dataManager = CoreDataManagerService()
    }
}
