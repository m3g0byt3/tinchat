//
//  ProfileImageDownloadConfigurator.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

final class ProfileImageDownloadConfigurator: NSObject {

    // MARK: - IBOutlets and UI

    @IBOutlet private weak var profileImageDownloadController: ProfileImageDownloadController!

    // MARK: - Public API

    override func awakeFromNib() {
        super.awakeFromNib()
        // TODO: Resolve depеndencies using DI Assembly
        let provider = APIProvider<ShibeOnlineAPI>(configuration: .shortTimeouts)
        let decoder = JSONDecoder()
        let modelParser = CodableJSONParser<[Photo]>(decoder: decoder)
        let photoParser = PhotoParser()

        let photoService = ShibeOnlineService(
            provider: provider,
            modelParser: modelParser,
            photoParser: photoParser
        )

        profileImageDownloadController.photoService = photoService
    }
}
