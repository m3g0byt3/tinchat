//
//  ProfileViewConfigurator.swift
//  TinChat
//
//  Created by m3g0byt3 on 22/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

final class ProfileViewConfigurator: NSObject {

    // MARK: - IBOutlets and UI

    @IBOutlet private weak var profileViewController: ProfileViewController!

    // MARK: - Public API

    override func awakeFromNib() {
        super.awakeFromNib()
        // TODO: Resolve depеndencies using DI Assembly
        profileViewController.dataManager = CoreDataManagerService()
    }
}
