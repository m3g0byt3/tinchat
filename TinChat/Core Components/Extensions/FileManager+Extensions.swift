//
//  FileManager+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 30/09/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

extension FileManager {
    /// Documents URL for current user (if available).
    static var documentsURL: URL? {
        return try? FileManager.default.url(for: .documentDirectory,
                                            in: .userDomainMask,
                                            appropriateFor: nil,
                                            create: false)
    }
}
