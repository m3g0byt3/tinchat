//
//  NSManagedObjectContext+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 04/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObjectContext {
    @discardableResult
    func saveOrRollback() -> Bool {
        do {
            if hasChanges {
                try save()
            }

            return true
        } catch {
            rollback()

            #if DEBUG
            print("Saving error: \(error.localizedDescription)")
            #endif

            return false
        }
    }
}
