//
//  String+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 29/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

extension String {
    var base64Encoded: String? {
        return self.data(using: .utf8)?.base64EncodedString()
    }
}
