//
//  NSFetchRequest+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 11/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import CoreData

// NSFetchRequest templates
extension NSFetchRequest where ResultType == ConversationMO {
    static func conversation(withIdentifier identifier: String) -> NSFetchRequest<ConversationMO> {
        let request: NSFetchRequest<ConversationMO> = ConversationMO.fetchRequest()

        request.predicate = NSPredicate(format: "\(#keyPath(ConversationMO.id)) == %@", identifier)
        request.fetchLimit = 1

        return request
    }

    static func nonEmptyOnlineConversations() -> NSFetchRequest<ConversationMO> {
        let request: NSFetchRequest<ConversationMO> = ConversationMO.fetchRequest()
        let format = "\(#keyPath(ConversationMO.user.isOnline)) == true AND \(#keyPath(ConversationMO.messages)).@count > 0"
        // swiftlint:disable:previous line_length
        request.predicate = NSPredicate(format: format)

        return request
    }
}

extension NSFetchRequest where ResultType == UserMO {
    static func onlineUsers() -> NSFetchRequest<UserMO> {
        let request: NSFetchRequest<UserMO> = UserMO.fetchRequest()

        request.predicate = NSPredicate(format: "\(#keyPath(UserMO.isOnline)) == true")

        return request
    }

    static func user(withIdentifier identifier: String) -> NSFetchRequest<UserMO> {
        let request: NSFetchRequest<UserMO> = UserMO.fetchRequest()

        request.predicate = NSPredicate(format: "\(#keyPath(UserMO.id)) == %@", identifier)
        request.fetchLimit = 1

        return request
    }
}

extension NSFetchRequest where ResultType == MessageMO {
    static func messagesFromConversation(withIdentifier identifier: String) -> NSFetchRequest<MessageMO> {
        let request: NSFetchRequest<MessageMO> = MessageMO.fetchRequest()

        request.predicate = NSPredicate(format: "\(#keyPath(MessageMO.conversation.id)) == %@", identifier)

        return request
    }
}
