//
//  URLSessionConfiguration+Extensions.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

extension URLSessionConfiguration {
    static let shortTimeouts: URLSessionConfiguration = { this in
        this.timeoutIntervalForRequest = Constants.Networking.requestTimeout
        this.timeoutIntervalForResource = Constants.Networking.resourceTimeout
        return this
    }(URLSessionConfiguration.default)
}
