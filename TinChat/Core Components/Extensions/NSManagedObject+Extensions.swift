//
//  NSManagedObject+Extensions.swift
//  TinChat
//
//  Created by m3g0byt3 on 11/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

extension ConversationMO {
    var messagesSorted: [MessageMO] {
        guard let messages = self.messages?.allObjects as? [MessageMO] else { return [] }
        return messages.sorted()
    }
}

extension UserProfileMO {
    var imageBlobURL: URL? {
        guard
            let documentsURL = FileManager.documentsURL,
            let userAvatarFileName = userAvatarFileName
            else {
                return nil
        }
        return URL(string: userAvatarFileName, relativeTo: documentsURL)
    }
}

// MARK: - Comparable protocol conformance

extension MessageMO: Comparable {
    public static func < (lhs: MessageMO, rhs: MessageMO) -> Bool {
        switch (lhs.date, rhs.date) {
        case let (.some(lhsDate), .some(rhsDate)):
            return lhsDate < rhsDate
        default:
            return lhs.date != nil
        }
    }
}
