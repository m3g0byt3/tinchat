//
//  DataManagerProtocol.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Core component for user profile management.
protocol DataManagerProtocol: AnyObject {
    typealias SaveCompletion = (Result<Void, DataManagerError>) -> Void
    typealias LoadCompletion = (Result<UserProfile, DataManagerError>) -> Void

    func saveUserProfile(_ userProfile: UserProfile, _ completion: @escaping SaveCompletion)
    func loadUserProfile(_ completion: @escaping LoadCompletion)
}
