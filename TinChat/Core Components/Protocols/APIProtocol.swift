//
//  APIProtocol.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Moya-inspired strongly-typed API endpoints representation
protocol APIProtocol {
    var scheme: String { get }
    var base: String { get }
    var path: String { get }
    var queryItems: [URLQueryItem] { get }
}
