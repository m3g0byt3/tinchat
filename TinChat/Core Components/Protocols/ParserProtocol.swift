//
//  ParserProtocol.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Provides parsing and/or conversion between raw data and concrete type.
protocol ParserProtocol {
    associatedtype ModelType

    func parse(_ data: Data) -> ModelType?
}
