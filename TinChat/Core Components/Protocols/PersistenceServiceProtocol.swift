//
//  PersistenceServiceProtocol.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Core component for persistange storage.
protocol PersistenceServiceProtocol {
    func append(_ message: Message, toConversationWithIdentifier identifier: String)
    func peer(_ peer: Peer, setStatus status: Peer.Status)
    func conversationIdentifierForPeer(_ peer: Peer) -> String?
    func peerForConversationWithIdentifier(_ identifier: String) -> Peer?
}
