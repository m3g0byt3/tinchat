//
//  APIProviderProtocol.swift
//  TinChat
//
//  Created by m3g0byt3 on 09/12/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

typealias Completion = (Result<Data, NetworkError>) -> Void

protocol APIProviderProtocol {
    associatedtype APIType

    @discardableResult
    func performRequest(_ type: APIType, completion: @escaping Completion) -> NetworkToken?
}
