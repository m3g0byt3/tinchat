//
//  DataManagerError.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// User profile management error.
enum DataManagerError: LocalizedError {

    case noDocumentsURLAvailable
    case unableToWrite
    case unableToRead
    case unknown

    // MARK: - LocalizedError protocol conformance

    var errorDescription: String? {
        switch self {
        case .noDocumentsURLAvailable: return R.string.localizable.dataManagerNoDocumentsURL()
        case .unableToRead: return R.string.localizable.dataManagerUnableToRead()
        case .unableToWrite: return R.string.localizable.dataManagerUnableToWrite()
        case .unknown: return R.string.localizable.dataManagerUnknown()
        }
    }
}
