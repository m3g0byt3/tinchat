//
//  CommunicationServiceProtocol.swift
//  TinChat
//
//  Created by m3g0byt3 on 28/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Core component for message sending and receiving.
protocol CommunicationServiceProtocol: AnyObject {
    var delegate: CommunicationServiceDelegate? { get set }
    var online: Bool { get set }

    func send(_ message: Message, to peer: Peer)
}
