//
//  CommunicationServiceDelegate.swift
//  TinChat
//
//  Created by m3g0byt3 on 28/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// `CommunicationServiceProtocol` instance delegate.
protocol CommunicationServiceDelegate: AnyObject {
    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didFoundPeer peer: Peer
    )

    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didLostPeer peer: Peer
    )

    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didNotStartBrowsingForPeers error: Error
    )

    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didReceiveInviteFromPeer peer: Peer,
        invitationClosure: (Bool) -> Void
    )

    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didNotStartAdvertisingForPeers error: Error
    )

    func communicationService(
        _ communicationService: CommunicationServiceProtocol,
        didReceiveMessage message: Message,
        from peer: Peer
    )
}
