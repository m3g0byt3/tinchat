//
//  APIProvider.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Generic abstraction over `URLSession`.
final class APIProvider<T>: APIProviderProtocol where T: URLRepresentable {

    // MARK: - Private properties

    private let session: URLSession

    // MARK: - Initialization/Deinitialization

    init(configuration: URLSessionConfiguration = .default) {
        self.session = URLSession(configuration: configuration)
    }

    // MARK: - Public API

    @discardableResult
    func performRequest(_ type: T, completion: @escaping Completion) -> NetworkToken? {
        // Early exit if we are unable to extract URL
        guard let url = type.url else {
            DispatchQueue.main.async {
                completion(.failure(.url))
            }
            return nil
        }

        // Perform network request
        let task = session.dataTask(with: url) { data, response, error in
            let successStatusCodes = Constants.Networking.successStatusCodes

            // Handle response
            switch (data, response, error) {

            // Network error (e.g. no internet connection available)
            case (_, _, .some(let error)):
                DispatchQueue.main.async {
                    completion(.failure(.underlying(error)))
                }

            // Backend error (e.g. 404 status code)
            case (_, let response as HTTPURLResponse, _) where !(successStatusCodes ~= response.statusCode):
                DispatchQueue.main.async {
                    completion(.failure(.backend))
                }

            // Successfully receive data from the backend
            case (.some(let data), _, _):
                DispatchQueue.main.async {
                    completion(.success(data))
                }

            // Other errors
            default:
                DispatchQueue.main.async {
                    completion(.failure(.unknown))
                }
            }
        }

        task.resume()

        return NetworkToken(task: task)
    }
}
