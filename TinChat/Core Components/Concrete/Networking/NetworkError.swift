//
//  NetworkError.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

// TODO: Separate network errors for Core and Service layers (if needed)
enum NetworkError: Error {
    case underlying(Error)
    case url
    case mapping
    case backend
    case unknown
}

// MARK: - LocalizedError protocol conformance

extension NetworkError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .underlying(let error): return error.localizedDescription
        case .url: return "Unable to construct URL."
        case .mapping: return "Unable to parse data from server."
        case .backend: return "Unable to get data from server."
        case .unknown: return "An unknown error occurred."
        }
    }
}
