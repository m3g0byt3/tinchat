//
//  NetworkToken.swift
//  
//
//  Created by Andrey Fedorov on 28/11/2018.
//

import Foundation

/// Allow to cancel associated network task.
final class NetworkToken {

    // MARK: - Private properties

    private weak var task: URLSessionTask?

    // MARK: - Initialization/Deinitialization

    init(task: URLSessionTask) {
        self.task = task
    }

    // MARK: - Public methods

    func cancel() {
        task?.cancel()
    }
}
