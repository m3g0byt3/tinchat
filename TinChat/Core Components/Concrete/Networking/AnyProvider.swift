//
//  AnyProvider.swift
//  TinChat
//
//  Created by m3g0byt3 on 09/12/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Type-erasure wrapper for `APIProviderProtocol` protocol.
final class AnyProvider<APIType> {

    // MARK: - Private properties

    private let performRequest: (_ ofType: APIType, _ completion: @escaping Completion) -> NetworkToken?

    // MARK: - Initialization/Deinitialization

    init<ProviderType: APIProviderProtocol>(
        _ provider: ProviderType
    )
        where ProviderType.APIType == APIType
    {
        // swiftlint:disable:previous opening_brace
        self.performRequest = provider.performRequest
    }
}

// MARK: - APIProviderProtocol protocol conformance

extension AnyProvider: APIProviderProtocol {
    @discardableResult
    func performRequest(_ type: APIType, completion: @escaping Completion) -> NetworkToken? {
        return performRequest(type, completion)
    }
}
