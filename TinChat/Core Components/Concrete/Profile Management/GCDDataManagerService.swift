//
//  GCDDataManagerService.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// `DataManagerProtocol` implementation using GCD.
@available(*, deprecated, message: "Use `CoreDataManagerService` instead")
final class GCDDataManagerService {

    // MARK: - Private properties

    private let queue = DispatchQueue(label: "com.m3g0byt3.GCDDataManagerServiceQueue",
                                      qos: .utility,
                                      attributes: .concurrent)
}

// MARK: - DataManagerProtocol protocol conformance

extension GCDDataManagerService: DataManagerProtocol {

    func saveUserProfile(_ userProfile: UserProfile, _ completion: @escaping DataManagerProtocol.SaveCompletion) {
        guard let documentsURL = FileManager.documentsURL else {
            completion(.failure(.noDocumentsURLAvailable))
            return
        }

        queue.async(flags: .barrier) {
            do {
                let avatarURL = documentsURL.appendingPathComponent(Constants.FileName.avatar)
                let usernameURL = documentsURL.appendingPathComponent(Constants.FileName.username)
                let descriptionURL = documentsURL.appendingPathComponent(Constants.FileName.description)

                try userProfile.imageBlob?.write(to: avatarURL)
                try userProfile.username?.data(using: .utf8)?.write(to: usernameURL)
                try userProfile.description?.data(using: .utf8)?.write(to: descriptionURL)

                DispatchQueue.main.async {
                    completion(.success)
                }
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(.unableToWrite))
                }
            }
        }
    }

    func loadUserProfile(_ completion: @escaping DataManagerProtocol.LoadCompletion) {
        guard let documentsURL = FileManager.documentsURL else {
            completion(.failure(.noDocumentsURLAvailable))
            return
        }

        queue.async {
            let avatarURL = documentsURL.appendingPathComponent(Constants.FileName.avatar)
            let usernameURL = documentsURL.appendingPathComponent(Constants.FileName.username)
            let descriptionURL = documentsURL.appendingPathComponent(Constants.FileName.description)

            let avatar = try? Data(contentsOf: avatarURL)
            let username = try? String(contentsOf: usernameURL)
            let description = try? String(contentsOf: descriptionURL)
            let profile = UserProfile(username: username, description: description, imageBlob: avatar)

            DispatchQueue.main.async {
                completion(.success(profile))
            }
        }
    }
}
