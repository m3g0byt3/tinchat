//
//  SaveProfileOperation.swift
//  TinChat
//
//  Created by m3g0byt3 on 22/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

final class SaveProfileOperation: Operation {

    // MARK: - Public properties

    var output: Result<Void, DataManagerError>?

    // MARK: - Private properties

    private let baseURL: URL
    private let userProfile: UserProfile

    // MARK: - Initialization/Deinitialization

    init(baseURL: URL, userProfile: UserProfile) {
        self.baseURL = baseURL
        self.userProfile = userProfile
    }

    // MARK: - Overrides

    override func main() {
        do {
            let avatarURL = baseURL.appendingPathComponent(Constants.FileName.avatar)
            let usernameURL = baseURL.appendingPathComponent(Constants.FileName.username)
            let descriptionURL = baseURL.appendingPathComponent(Constants.FileName.description)

            try userProfile.imageBlob?.write(to: avatarURL)
            try userProfile.username?.data(using: .utf8)?.write(to: usernameURL)
            try userProfile.description?.data(using: .utf8)?.write(to: descriptionURL)

            output = .success
        } catch {
            output = .failure(.unableToWrite)
        }
    }
}
