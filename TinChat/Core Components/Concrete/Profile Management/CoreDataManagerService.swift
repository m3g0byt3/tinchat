//
//  CoreDataManagerService.swift
//  TinChat
//
//  Created by m3g0byt3 on 04/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// `DataManagerProtocol` implementation using CoreData.
final class CoreDataManagerService {

    // MARK: - Types

    private enum ProfilePermission {
        case readOnly
        case readWrite
    }

    // MARK: - Private properties

    /// This private queue still required even separate NSManagedObjectContext CoreData contexts used
    /// since we are doing potentially heavy-load operations like file reading/writing.
    private let queue = DispatchQueue(label: "com.m3g0byt3.CoreDataManagerService",
                                      qos: .utility,
                                      attributes: .concurrent)

    // MARK: - Private API

    /// Returns new or already existed `UserProfileMO` instance.
    /// - Parameter permission: Determines NSManagedObjectContext (`.mainContext` or `.privateContext`) to use.
    /// - Returns: New or already existed `UserProfileMO` instance.
    private func getProfile(withPermission permission: ProfilePermission) -> UserProfileMO? {
        let context = permission == .readOnly ? CoreDataStack.shared.mainContext : CoreDataStack.shared.privateContext
        // Retrieve existing managed object...
        if let profiles = try? context.fetch(UserProfileMO.fetchRequest()) as? [UserProfileMO],
            let profile = profiles?.first {
            return profile
        }
        // ...Or create new one
        let profile = UserProfileMO(context: context)
        guard context.saveOrRollback() else {
            return nil
        }
        return profile
    }
}

// MARK: - DataManagerProtocol protocol conformance

extension CoreDataManagerService: DataManagerProtocol {

    func saveUserProfile(_ userProfile: UserProfile, _ completion: @escaping DataManagerProtocol.SaveCompletion) {
        queue.async(flags: .barrier) {
            guard let currentProfile = self.getProfile(withPermission: .readWrite) else {
                DispatchQueue.main.async {
                    completion(.failure(.unableToWrite))
                }
                return
            }

            userProfile.username.map { currentProfile.userName = $0 }
            userProfile.description.map { currentProfile.userDescription = $0 }

            if let imageBlob = userProfile.imageBlob {
                currentProfile.imageBlobURL.flatMap { try? FileManager.default.removeItem(at: $0) }
                currentProfile.userAvatarFileName = UUID().uuidString
                currentProfile.imageBlobURL.flatMap { try? imageBlob.write(to: $0) }
            }

            if let context = currentProfile.managedObjectContext, context.saveOrRollback() {
                DispatchQueue.main.async {
                    completion(.success)
                }
            } else {
                DispatchQueue.main.async {
                    completion(.failure(.unableToWrite))
                }
            }
        }
    }

    func loadUserProfile(_ completion: @escaping DataManagerProtocol.LoadCompletion) {
        queue.async {
            if let currentProfile = self.getProfile(withPermission: .readOnly) {
                let userProfile = UserProfile(
                    username: currentProfile.userName,
                    description: currentProfile.userDescription,
                    imageBlob: currentProfile.imageBlobURL.flatMap { try? Data(contentsOf: $0) }
                )
                DispatchQueue.main.async {
                    completion(.success(userProfile))
                }
            } else {
                DispatchQueue.main.async {
                    completion(.failure(.unableToRead))
                }
            }
        }
    }
}
