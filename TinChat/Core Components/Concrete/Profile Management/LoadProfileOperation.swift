//
//  LoadProfileOperation.swift
//  TinChat
//
//  Created by m3g0byt3 on 22/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

final class LoadProfileOperation: Operation {

    // MARK: - Public properties

    var output: UserProfile?

    // MARK: - Private properties

    private let baseURL: URL

    // MARK: - Initialization/Deinitialization

    init(baseURL: URL) {
        self.baseURL = baseURL
    }

    // MARK: - Overrides

    override func main() {
        let avatarURL = baseURL.appendingPathComponent(Constants.FileName.avatar)
        let usernameURL = baseURL.appendingPathComponent(Constants.FileName.username)
        let descriptionURL = baseURL.appendingPathComponent(Constants.FileName.description)

        let avatar = try? Data(contentsOf: avatarURL)
        let username = try? String(contentsOf: usernameURL)
        let description = try? String(contentsOf: descriptionURL)

        output = UserProfile(username: username, description: description, imageBlob: avatar)
    }
}
