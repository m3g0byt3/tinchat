//
//  OperationsDataManagerService.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// `DataManagerProtocol` implementation using NSOperation.
@available(*, deprecated, message: "Use `CoreDataManagerService` instead")
final class OperationsDataManagerService {

    // MARK: - Private properties

    private let queue: OperationQueue = { this in
        this.qualityOfService = .utility
        this.maxConcurrentOperationCount = 10
        this.name = "com.m3g0byt3.OperationsDataManagerServiceQueue"
        return this
    }(OperationQueue())
}

// MARK: - DataManagerProtocol protocol conformance

extension OperationsDataManagerService: DataManagerProtocol {

    func saveUserProfile(_ userProfile: UserProfile, _ completion: @escaping DataManagerProtocol.SaveCompletion) {
        guard let documentsURL = FileManager.documentsURL else {
            completion(.failure(.noDocumentsURLAvailable))
            return
        }

        let operation = SaveProfileOperation(baseURL: documentsURL, userProfile: userProfile)

        operation.completionBlock = {
            guard let result = operation.output else {
                OperationQueue.main.addOperation { completion(.failure(.unableToWrite)) }
                return
            }

            switch result {
            case .success:
                OperationQueue.main.addOperation { completion(.success) }
            case .failure:
                OperationQueue.main.addOperation { completion(.failure(.unableToWrite)) }
            }
        }

        queue.addOperation(operation)
    }

    func loadUserProfile(_ completion: @escaping DataManagerProtocol.LoadCompletion) {
        guard let documentsURL = FileManager.documentsURL else {
            completion(.failure(.noDocumentsURLAvailable))
            return
        }

        let operation = LoadProfileOperation(baseURL: documentsURL)

        operation.completionBlock = {
            guard let profile = operation.output else {
                OperationQueue.main.addOperation { completion(.failure(.unableToRead)) }
                return
            }
            OperationQueue.main.addOperation { completion(.success(profile)) }
        }

        queue.addOperation(operation)
    }
}
