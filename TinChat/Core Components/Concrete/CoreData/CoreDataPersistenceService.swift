//
//  CoreDataPersistenceService.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import CoreData

/// `PersistenceServiceProtocol` implementation using CoreData.
final class CoreDataPersistenceService {

    // MARK: - Initialization/Deinitialization

    init() {
        self.prepareExistingData()
    }

    // MARK: - Private API

    private func prepareExistingData() {
        CoreDataStack.shared.mainContext.perform {
            let request: NSFetchRequest<UserMO> = UserMO.fetchRequest()
            if let users = try? CoreDataStack.shared.mainContext.fetch(request) {
                users.forEach { $0.isOnline = false }
            }

            CoreDataStack.shared.mainContext.saveOrRollback()
        }
    }
}

// MARK: - PersistenceServiceProtocol protocol conformance

extension CoreDataPersistenceService: PersistenceServiceProtocol {
    func append(_ message: Message, toConversationWithIdentifier identifier: String) {
        CoreDataStack.shared.mainContext.perform {
            let request = NSFetchRequest.conversation(withIdentifier: identifier)
            guard let conversations = try? CoreDataStack.shared.mainContext.fetch(request) else { return }
            let messageMO = MessageMO(context: CoreDataStack.shared.mainContext, message: message)

            conversations.first?.addToMessages(messageMO)
            conversations.first?.hasUnreadMessages = message.direction == .inbound

            CoreDataStack.shared.mainContext.saveOrRollback()
        }
    }

    func conversationIdentifierForPeer(_ peer: Peer) -> String? {
        let request = NSFetchRequest.user(withIdentifier: peer.identifier)
        guard let users = try? CoreDataStack.shared.mainContext.fetch(request) else {
            return nil
        }

        return users.first?.conversation?.id
    }

    func peerForConversationWithIdentifier(_ identifier: String) -> Peer? {
        let request = NSFetchRequest.conversation(withIdentifier: identifier)
        guard
            let conversations = try? CoreDataStack.shared.mainContext.fetch(request),
            let user = conversations.first?.user
        else { return nil }

        return Peer(model: user)
    }

    func peer(_ peer: Peer, setStatus status: Peer.Status) {
        CoreDataStack.shared.mainContext.perform {
            let request = NSFetchRequest.user(withIdentifier: peer.identifier)
            let user: UserMO?

            if let users = try? CoreDataStack.shared.mainContext.fetch(request), !users.isEmpty {
                user = users.first
            } else {
                let conversationMO = ConversationMO(context: CoreDataStack.shared.mainContext)

                user = UserMO(context: CoreDataStack.shared.mainContext)

                conversationMO.user = user
                conversationMO.id = (peer.name + peer.identifier).base64Encoded
                user?.id = peer.identifier
                user?.name = peer.name
            }

            user?.isOnline = status.isOnline

            CoreDataStack.shared.mainContext.saveOrRollback()
        }
    }
}

// MARK: - Convenience extensions

private extension Peer {
    init?(model: UserMO) {
        guard let name = model.name, let identifier = model.id else { return nil }
        self.name = name
        self.identifier = identifier
    }
}

private extension MessageMO {
    convenience init(context: NSManagedObjectContext, message: Message) {
        self.init(context: context)
        self.id = message.identifier
        self.date = message.date
        self.text = message.text
        self.rawDirection = message.direction.rawValue
        self.rawType = message.type.rawValue
    }
}

private extension Peer.Status {
    var isOnline: Bool {
        return self == .online
    }
}
