//
//  CoreDataStack.swift
//  TinChat
//
//  Created by m3g0byt3 on 04/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import CoreData

/// Multi-context CoreData stack.
final class CoreDataStack {

    // MARK: - Class properties

    static let shared: CoreDataStack = {
        do {
            return try CoreDataStack()
        } catch {
            fatalError("CoreData stack setup failed: \(error.localizedDescription)")
        }
    }()

    // MARK: - Public properties

    let mainContext: NSManagedObjectContext
    let privateContext: NSManagedObjectContext

    // MARK: - Private properties

    private var token: NSObjectProtocol?

    // MARK: - Initialization/Deinitialization

    private init() throws {
        // Find model file in bundle
        guard let modelURL = Bundle.main.url(
            forResource: Constants.FileName.modelName,
            withExtension: Constants.FileName.modelExtension
        ) else {
            throw CoreDataStackError.modelFile
        }

        // Setup model
        guard let objectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            throw CoreDataStackError.objectModel
        }

        // Get database URL
        guard
            let documentsURL = FileManager.documentsURL,
            let databaseURL = URL(string: Constants.FileName.databaseName, relativeTo: documentsURL)
        else {
            throw CoreDataStackError.databaseURL
        }

        // Setup coordinator
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: objectModel)
        do {
            try coordinator.addPersistentStore(
                ofType: NSSQLiteStoreType,
                configurationName: nil,
                at: databaseURL,
                options: nil
            )
        } catch {
            throw CoreDataStackError.loadPersistentStore
        }

        // Setup contexts
        self.mainContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        self.privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        [self.mainContext, self.privateContext].forEach { $0.persistentStoreCoordinator = coordinator }

        // Setup inter-context merge
        self.setupObservers()
    }

    deinit {
        token.map(NotificationCenter.default.removeObserver)
    }

    // MARK: - Private API

    private func setupObservers() {
        let center = NotificationCenter.default
        let name: Notification.Name = .NSManagedObjectContextDidSave

        mainContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump

        token = center.addObserver(forName: name, object: privateContext, queue: nil) { [weak self] note in
            // FIXME: Fix crash on merge from private to main context:
            // https://gist.github.com/m3g0byt3/571fed6b4253f56afa6f0ac42fcfde04
            self?.mainContext.mergeChanges(fromContextDidSave: note)
        }
    }
}
