//
//  CoreDataStackError.swift
//  TinChat
//
//  Created by m3g0byt3 on 04/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// CoreData stack initialization error.
enum CoreDataStackError: LocalizedError {
    case modelFile
    case objectModel
    case databaseURL
    case loadPersistentStore

    // MARK: - LocalizedError protocol conformance

    var errorDescription: String? {
        switch self {
        case .modelFile: return R.string.localizable.coreDataStackModelFile()
        case .objectModel: return R.string.localizable.coreDataStackObjectModel()
        case .databaseURL: return R.string.localizable.coreDataStackDatabaseUrl()
        case .loadPersistentStore: return R.string.localizable.coreDataLoadPersistentStore()
        }
    }
}
