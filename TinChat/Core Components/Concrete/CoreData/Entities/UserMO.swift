//
//  UserMO.swift
//  TinChat
//
//  Created by m3g0byt3 on 12/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//
// swiftlint:disable identifier_name

import Foundation
import CoreData

final class UserMO: NSManagedObject {
    @nonobjc class func fetchRequest() -> NSFetchRequest<UserMO> {
        return NSFetchRequest<UserMO>(entityName: String(describing: self))
    }

    @NSManaged var id: String?
    @NSManaged var name: String?
    @NSManaged var conversation: ConversationMO?
    @NSManaged var isOnline: Bool

    override func willChangeValue(forKey key: String) {
        super.willChangeValue(forKey: key)
        if key == #keyPath(UserMO.isOnline) {
            conversation?.willChangeValue(forKey: #keyPath(ConversationMO.user))
        }
    }

    override func didChangeValue(forKey key: String) {
        super.didChangeValue(forKey: key)
        if key == #keyPath(UserMO.isOnline) {
            conversation?.didChangeValue(forKey: #keyPath(ConversationMO.user))
        }
    }
}
