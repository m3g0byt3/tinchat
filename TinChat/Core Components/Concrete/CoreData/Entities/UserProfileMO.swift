//
//  UserProfileMO.swift
//  TinChat
//
//  Created by m3g0byt3 on 04/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import CoreData

final class UserProfileMO: NSManagedObject {
    @nonobjc class func fetchRequest() -> NSFetchRequest<UserProfileMO> {
        return NSFetchRequest<UserProfileMO>(entityName: String(describing: self))
    }

    @NSManaged var userName: String?
    @NSManaged var userDescription: String?
    @NSManaged var userAvatarFileName: String?
}
