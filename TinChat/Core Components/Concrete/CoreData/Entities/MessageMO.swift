//
//  MessageMO.swift
//  TinChat
//
//  Created by m3g0byt3 on 12/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//
// swiftlint:disable identifier_name

import Foundation
import CoreData

final class MessageMO: NSManagedObject {
    @nonobjc class func fetchRequest() -> NSFetchRequest<MessageMO> {
        return NSFetchRequest<MessageMO>(entityName: String(describing: self))
    }

    var direction: Message.Direction? {
        return Message.Direction(rawValue: rawDirection)
    }

    @NSManaged var date: Date?
    @NSManaged var rawDirection: Int16
    @NSManaged var id: String?
    @NSManaged var text: String?
    @NSManaged var rawType: String?
    @NSManaged var conversation: ConversationMO?
}
