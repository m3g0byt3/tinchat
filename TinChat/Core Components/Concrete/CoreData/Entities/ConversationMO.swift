//
//  ConversationMO.swift
//  TinChat
//
//  Created by m3g0byt3 on 12/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//
// swiftlint:disable identifier_name

import Foundation
import CoreData

final class ConversationMO: NSManagedObject {
    @nonobjc class func fetchRequest() -> NSFetchRequest<ConversationMO> {
        return NSFetchRequest<ConversationMO>(entityName: String(describing: self))
    }

    @NSManaged var hasUnreadMessages: Bool
    @NSManaged var id: String?
    @NSManaged var messages: NSSet?
    @NSManaged var user: UserMO?
}

extension ConversationMO {

    @objc(addMessagesObject:)
    @NSManaged func addToMessages(_ value: MessageMO)

    @objc(removeMessagesObject:)
    @NSManaged func removeFromMessages(_ value: MessageMO)

    @objc(addMessages:)
    @NSManaged func addToMessages(_ values: NSSet)

    @objc(removeMessages:)
    @NSManaged func removeFromMessages(_ values: NSSet)
}
