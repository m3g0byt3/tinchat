//
//  MCCommunicationService.swift
//  TinChat
//
//  Created by m3g0byt3 on 28/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import MultipeerConnectivity

/// `CommunicationServiceProtocol` implementation using MultipeerConnectivity framework.
final class MCCommunicationService: NSObject {

    // MARK: - Public Properties

    weak var delegate: CommunicationServiceDelegate?

    var online: Bool {
        didSet {
            updateStatus()
        }
    }

    // MARK: - Private Properties

    private let advertiser: MCNearbyServiceAdvertiser
    private let browser: MCNearbyServiceBrowser
    private let session: MCSession
    private let localPeer: MCPeerID
    private let encoder: JSONEncoder
    private let decoder: JSONDecoder

    // MARK: - Initialization/Deinitialization

    init(delegate: CommunicationServiceDelegate? = nil) {
        self.encoder = JSONEncoder()
        self.decoder = JSONDecoder()
        self.delegate = delegate
        // TODO: Retrive saved display name instead
        self.localPeer = MCPeerID(displayName: "\(Constants.CommunicationService.displayName)@\(UIDevice.current.name)")
        self.advertiser = MCNearbyServiceAdvertiser(peer: self.localPeer,
                                                    discoveryInfo: Constants.CommunicationService.discoveryInfo,
                                                    serviceType: Constants.CommunicationService.serviceType)
        self.browser = MCNearbyServiceBrowser(peer: self.localPeer,
                                              serviceType: Constants.CommunicationService.serviceType)
        self.session = MCSession(peer: self.localPeer)
        self.online = false

        super.init()

        self.advertiser.delegate = self
        self.browser.delegate = self
        self.session.delegate = self
    }

    // MARK: - Private API

    private func updateStatus() {
        if online {
            advertiser.startAdvertisingPeer()
            browser.startBrowsingForPeers()
        } else {
            advertiser.stopAdvertisingPeer()
            advertiser.stopAdvertisingPeer()
        }
    }
}

// MARK: - CommunicationServiceProtocol protocol conformance

extension MCCommunicationService: CommunicationServiceProtocol {
    func send(_ message: Message, to peer: Peer) {
        guard let peerID = session.connectedPeers.first(where: { $0.displayName == peer.name }) else { return }

        do {
            let data = try encoder.encode(message)
            try session.send(data, toPeers: [peerID], with: .reliable)
        } catch {
            Logger.log(at: .error, "An error has occured: \(error.localizedDescription).")
        }
    }
}

// MARK: - MCNearbyServiceAdvertiserDelegate protocol conformance

extension MCCommunicationService: MCNearbyServiceAdvertiserDelegate {
    func advertiser(
        _ advertiser: MCNearbyServiceAdvertiser,
        didReceiveInvitationFromPeer peerID: MCPeerID,
        withContext context: Data?,
        invitationHandler: @escaping (Bool, MCSession?) -> Void
    ) {
        let peer = Peer(from: peerID)

        DispatchQueue.main.async {
            self.delegate?.communicationService(self, didReceiveInviteFromPeer: peer) { accepted in
            invitationHandler(accepted, self.session)
            }
        }
    }

    func advertiser(
        _ advertiser: MCNearbyServiceAdvertiser,
        didNotStartAdvertisingPeer error: Error
    ) {
        DispatchQueue.main.async {
            self.delegate?.communicationService(self, didNotStartAdvertisingForPeers: error)
        }
    }
}

// MARK: - MCNearbyServiceBrowserDelegate protocol conformance

extension MCCommunicationService: MCNearbyServiceBrowserDelegate {
    func browser(
        _ browser: MCNearbyServiceBrowser,
        foundPeer peerID: MCPeerID,
        withDiscoveryInfo info: [String: String]?
    ) {
        if !session.connectedPeers.contains(peerID) {
            browser.invitePeer(peerID, to: session, withContext: nil, timeout: Constants.CommunicationService.timeout)
        } else {
            let peer = Peer(from: peerID)

            DispatchQueue.main.async {
                self.delegate?.communicationService(self, didFoundPeer: peer)
            }
        }
    }

    func browser(
        _ browser: MCNearbyServiceBrowser,
        lostPeer peerID: MCPeerID
    ) {
        let peer = Peer(from: peerID)

        DispatchQueue.main.async {
            self.delegate?.communicationService(self, didLostPeer: peer)
        }
    }

    func browser(
        _ browser: MCNearbyServiceBrowser,
        didNotStartBrowsingForPeers error: Error
    ) {
        DispatchQueue.main.async {
            self.delegate?.communicationService(self, didNotStartBrowsingForPeers: error)
        }
    }
}

// MARK: - MCSessionDelegate protocol conformance

extension MCCommunicationService: MCSessionDelegate {
    func session(
        _ session: MCSession,
        peer peerID: MCPeerID,
        didChange state: MCSessionState
    ) {
        let peer = Peer(from: peerID)

        DispatchQueue.main.async {
            switch state {
            case .connected:
                self.delegate?.communicationService(self, didFoundPeer: peer)
            case .notConnected:
                self.delegate?.communicationService(self, didLostPeer: peer)
            case .connecting:
                break
            }
        }
    }

    func session(
        _ session: MCSession,
        didReceive data: Data,
        fromPeer peerID: MCPeerID
    ) {
        guard let message = try? decoder.decode(Message.self, from: data) else { return }
        let peer = Peer(from: peerID)

        DispatchQueue.main.async {
            self.delegate?.communicationService(self, didReceiveMessage: message, from: peer)
        }
    }

    func session(
        _ session: MCSession,
        didReceive stream: InputStream,
        withName streamName: String,
        fromPeer peerID: MCPeerID
    ) {
        Logger.log(at: .notification, "\(#function) is not implemented.")
    }

    func session(
        _ session: MCSession,
        didStartReceivingResourceWithName resourceName: String,
        fromPeer peerID: MCPeerID,
        with progress: Progress
    ) {
        Logger.log(at: .notification, "\(#function) is not implemented.")
    }

    func session(
        _ session: MCSession,
        didFinishReceivingResourceWithName resourceName: String,
        fromPeer peerID: MCPeerID,
        at localURL: URL?,
        withError error: Error?
    ) {
        Logger.log(at: .notification, "\(#function) is not implemented.")
    }
}

// MARK: - Convenience extension

private extension Peer {
    init(from peerID: MCPeerID) {
        self.name = peerID.displayName
        self.identifier = peerID.displayName.base64Encoded!
        // swiftlint:disable:previous force_unwrapping
    }
}
