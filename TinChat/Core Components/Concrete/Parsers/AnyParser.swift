//
//  AnyParser.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Type-erasure wrapper for `ParserProtocol` protocol.
final class AnyParser<ModelType> {

    // MARK: - Private properties

    private let parse: (Data) -> ModelType?

    // MARK: - Initialization/Deinitialization

    init<ParserType: ParserProtocol>(_ parser: ParserType) where ParserType.ModelType == ModelType {
        self.parse = parser.parse
    }
}

// MARK: - ParserProtocol protocol conformance

extension AnyParser: ParserProtocol {
    func parse(_ data: Data) -> ModelType? {
        return parse(data)
    }
}
