//
//  CodableJSONParser.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Parse data using Apple's `Decodable` `JSONDecoder`.
final class CodableJSONParser<T> where T: Decodable {

    // MARK: - Private properties

    private let decoder: JSONDecoder

    // MARK: - Initialization/Deinitialization

    init(decoder: JSONDecoder) {
        self.decoder = decoder
    }
}

// MARK: - ParserProtocol protocol conformance

extension CodableJSONParser: ParserProtocol {
    func parse(_ data: Data) -> T? {
        return try? decoder.decode(T.self, from: data)
    }
}
