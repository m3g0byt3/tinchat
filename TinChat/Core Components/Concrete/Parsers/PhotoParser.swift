//
//  PhotoParser.swift
//  TinChat
//
//  Created by Andrey Fedorov on 27/11/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

/// Converts data to the image and wraps result to the opaque wrapper (a `PhotoParser` instance).
final class PhotoParser {}

// MARK: - ParserProtocol protocol conformance

extension PhotoParser: ParserProtocol {
    func parse(_ data: Data) -> PhotoWrapper? {
        return PhotoWrapper(imageBlob: data)
    }
}
