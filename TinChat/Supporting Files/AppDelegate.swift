//
//  AppDelegate.swift
//  TinChat
//
//  Created by m3g0byt3 on 23/09/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Public properties

    var window: UIWindow?

    // MARK: - UIApplicationDelegate protocol conformance

    func application(
        _ application: UIApplication,
        willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]? = nil
    ) -> Bool {
        setupCoreData()
        setupOverlayWindow()

        return true
    }

    // MARK: - Private API

    private func setupCoreData() {
        _ = CoreDataStack.shared
    }

    private func setupOverlayWindow() {
        window = OverlayPassthroughWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = R.storyboard.conversationListViewController.instantiateInitialViewController()
        window?.makeKeyAndVisible()
    }
}
