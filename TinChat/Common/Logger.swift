//
//  Logger.swift
//  TinChat
//
//  Created by m3g0byt3 on 23/09/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
import QuartzCore

enum Logger {

    // MARK: - Private properties

    private static var interval = CACurrentMediaTime()

    private static let dateFormatter: DateFormatter = { this in
        this.dateFormat = "dd/MM/yyyy HH:mm:ss.SSS"
        return this
    }(DateFormatter())

    private static let numberFormatter: NumberFormatter = { this in
        this.numberStyle = .decimal
        this.minimumFractionDigits = 10
        return this
    }(NumberFormatter())

    // MARK: - Public API

    static func log(at severity: Severity, _ message: @autoclosure () -> String = #function) {
        defer { Logger.interval = CACurrentMediaTime() }

        let delta = CACurrentMediaTime() - Logger.interval
        let interval = delta > 0 ? delta : 0
        let wrappedInterval = NSNumber(value: interval)
        let intervalString = Logger.numberFormatter.string(from: wrappedInterval) ?? "0"
        let dateString = Logger.dateFormatter.string(from: Date())
        let severityString = severity.rawValue

        print("\(severityString)\(dateString) +\(intervalString): \(message())")
    }
}
