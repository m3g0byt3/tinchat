//
//  Severity.swift
//  TinChat
//
//  Created by m3g0byt3 on 23/09/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

enum Severity: String {

    case emergency = "❤️"
    case alert = "💖"
    case critical = "🧡"
    case error = "💛"
    case warning = "💜"
    case notification = "💙"
    case information = "💚"
    case debug = "🖤"
}
