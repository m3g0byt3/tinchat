//
//  Result.swift
//  TinChat
//
//  Created by m3g0byt3 on 21/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation

enum Result<T, E: Error> {

    case success(T)
    case failure(E)
}

extension Result where T == Void {

    static var success: Result {
        return .success(())
    }
}
