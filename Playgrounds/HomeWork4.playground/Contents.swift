//
//  HomeWork4.playground
//  TinChat
//
//  Created by m3g0byt3 on 14/10/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//
// swiftlint:disable line_length

import Foundation

// MARK: - Helpers

extension String {
    static var placeholder: String {
        return Array(repeating: "=", count: 130).joined()
    }
    var escaped: String {
        return "\"\(self)\""
    }
}

// MARK: - Abstract classes

/// Represents employee type.
enum EmployeeType: String {
    case chiefExecutiveOfficer = "Chief Executive Officer"
    case productManager = "Product Manager"
    case softwareDeveloper = "Software Developer"
}

/// An abstract class, represents destructable object.
class Object {
    init() {
        // Avoid initialization of abstract class
        guard type(of: self) != Object.self else {
            fatalError("Create a subclass instance of abstract class \(Object.self).")
        }
    }

    deinit {
        print("\(self) has been destroyed.")
    }
}

/// An abstract class, represents concrete worker.
class Employee: Object, CustomStringConvertible {
    let title: EmployeeType
    let firstName: String
    let lastName: String
    var displayName: String {
        return "\(firstName) \(lastName)"
    }
    var description: String {
        return "\(title.rawValue) \(displayName.escaped)"
    }

    init(title: EmployeeType, firstName: String, lastName: String) {
        // Avoid initialization of abstract class
        guard type(of: self) != Employee.self else {
            fatalError("Create a subclass instance of abstract class \(Employee.self).")
        }
        self.title = title
        self.firstName = firstName
        self.lastName = lastName
        super.init()
    }
}

// MARK: - Concrete classes

final class Company: Object, CustomStringConvertible {
    let name: String
    let ceo: CEO
    var description: String {
        return "Company \(name.escaped)"
    }

    init(name: String, ceo: CEO) {
        self.ceo = ceo
        self.name = name
        super.init()
    }
}

final class CEO: Employee {
    typealias Closure = () -> Void

    weak var company: Company?
    let productManager: ProductManager
    var talkToProductManager: Closure?
    var printProductManager: Closure?
    var printAllDevelopers: Closure?

    init(firstName: String, lastName: String, productManager: ProductManager) {
        self.productManager = productManager
        super.init(title: .chiefExecutiveOfficer, firstName: firstName, lastName: lastName)
    }
}

final class ProductManager: Employee {
    weak var ceo: CEO?
    let softwareDevelopers: [SoftwareDeveloper]

    init(firstName: String, lastName: String, developers: [SoftwareDeveloper]) {
        self.softwareDevelopers = developers
        super.init(title: .productManager, firstName: firstName, lastName: lastName)
    }
}

final class SoftwareDeveloper: Employee {
    weak var productManager: ProductManager?

    init(firstName: String, lastName: String) {
        super.init(title: .softwareDeveloper, firstName: firstName, lastName: lastName)
    }

    func talkToOtherDeveloper(firstName: String, lastName: String, message: String) {
        guard let other = productManager?.softwareDevelopers.first(where: { $0.firstName == firstName && $0.lastName == lastName }) else {
            return
        }
        sendMessage(message, to: other)
    }

    func talkToProductManager(message: String) {
        guard let other = productManager else {
            assertionFailure("Product manager not set!")
            return
        }
        sendMessage(message, to: other)
    }

    func talkToCEO(message: String) {
        guard let other = productManager?.ceo else {
            assertionFailure("Product manager or CEO not set!")
            return
        }
        sendMessage(message, to: other)
    }

    private func sendMessage(_ message: String, to other: Employee) {
        let fromString = "\(title.rawValue) \(displayName.escaped)"
        let toString = "\(other.title.rawValue) \(other.displayName.escaped)"
        print("\(fromString) to \(toString): \(message.escaped)")
    }
}

// MARK: - Factory

func makeCompany() -> Company {
    let developers = (1...5).map { SoftwareDeveloper(firstName: "Developer", lastName: "#\($0)") }
    let productManager = ProductManager(firstName: "Smith", lastName: "Thompson", developers: developers)
    let ceo = CEO(firstName: "John", lastName: "Doe", productManager: productManager)
    let company = Company(name: "Apple", ceo: ceo)

    ceo.company = company
    productManager.ceo = ceo
    developers.forEach { $0.productManager = productManager }

    ceo.talkToProductManager = { [weak ceo] in
        guard let ceo = ceo, let company = ceo.company else {
            assertionFailure("Unable to unwrap!")
            return
        }
        print("Company \(company.name.escaped) hierarchy:")
        print("CEO: \(ceo.displayName.escaped)")
        print("Product Manager: \(productManager.displayName.escaped)")
        ceo.printAllDevelopers?()
    }

    ceo.printProductManager = { [weak productManager = ceo.productManager] in
        guard let productManager = productManager else {
            assertionFailure("Unable to unwrap!")
            return
        }
        print(productManager)
    }

    ceo.printAllDevelopers = { [weak productManager = ceo.productManager] in
        guard let productManager = productManager else {
            assertionFailure("Unable to unwrap!")
            return
        }
        let prettyPrinted = productManager.softwareDevelopers.map { $0.displayName.escaped }.joined(separator: ", ")
        print("Developers: \(prettyPrinted)")
    }

    return company
}

// MARK: - Main routine

func main() {
    let company = makeCompany()

    print("CEO's closures:")
    print("\nTalk to product manager closure:")
    company.ceo.talkToProductManager?()

    print("\nPrint product manager closure:")
    company.ceo.printProductManager?()

    print("\nPrint all developers closure:")
    company.ceo.printAllDevelopers?()

    print(String.placeholder)

    print("\nDevs' talks:")
    print("\nDevs to each other:")
    company.ceo.productManager.softwareDevelopers.first?.talkToOtherDeveloper(firstName: "Developer", lastName: "#2", message: "Hello buddy!")
    company.ceo.productManager.softwareDevelopers.last?.talkToOtherDeveloper(firstName: "Developer", lastName: "#3", message: "Please review my PR!")

    print("\nDevs to Product Manager:")
    company.ceo.productManager.softwareDevelopers.first?.talkToProductManager(message: "Hooray, the-incredible-hardly-reproduced-bug #666 was fixed!")
    company.ceo.productManager.softwareDevelopers.last?.talkToProductManager(message: "Could you please give me more task cuz I'm dying of boredom?")

    print("\nDevs to CEO:")
    company.ceo.productManager.softwareDevelopers.first?.talkToCEO(message: "What is the Swift 5.0 release date?")
    company.ceo.productManager.softwareDevelopers.last?.talkToCEO(message: "I want more money or I'll move to Google asap!")

    print(String.placeholder)

    print("\nThe end of the world!\n")
}

main()
