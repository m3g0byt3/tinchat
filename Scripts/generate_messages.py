#!/usr/bin/env python

import random
import argparse
import time

# Arg parse

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--path", required=True, help="Destination file path.")

# Variables

args = parser.parse_args()
max_time = int(time.time())
min_time = max_time - 60 * 60 * 24 * 3 # Three days
text_short = "L"
text_medium = "Lorem ipsum dolor sit amet, co"
text_long = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
texts = [text_short, text_medium, text_long]

# Functions

# Returns randomly generated message.
def getMessage(text, direction):
    return  """
    {{
        "date":{date},
        "direction":"{direction}",
        "text":"{message}"
    }},
            """.format(
                date = random.randint(min_time, max_time),  
                direction = direction,
                message = text
            )

# Main routine

if __name__ == '__main__':
    inbound_messages = list(map(lambda text: getMessage(text, "inbound"), texts))
    outbound_messages = list(map(lambda text: getMessage(text, "outbound"), texts))
    messages = inbound_messages + outbound_messages
    random.shuffle(messages)

    json = "[\n"
    for message in messages:
        json += message
    json += "\n]"
    json = json[::-1].replace(",","",1)[::-1]

    with open(args.path, "w") as file:
        file.write(json)
    