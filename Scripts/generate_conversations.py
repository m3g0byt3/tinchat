#!/usr/bin/env python

import random
import uuid
import argparse
import sys
import time
from last_names import last_names
from first_names import first_names

# Arg parse

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--path", required=True, help="Destination file path.")
parser.add_argument("-c", "--count", required=True, type=int, help="Number of generated conversations.")

# Variables

args = parser.parse_args()
max_time = int(time.time())
min_time = max_time - 60 * 60 * 24 * 1 # One day
message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

# Functions

# Returns randomly generated "true" or "false" string.
def getRandomFlag():
    return random.choice(["true", "false"])

# Returns randomly generated "outbound" or "inbound" string.
def getRandomDirection():
    return random.choice(["outbound", "inbound"])

# Returns randomly generated message or empty string with 1/seed probability.
def getMessage(seed):
    if random.randint(0, seed) % seed == 0:
        return ""
    return  """
                {{
                    "date":{date},
                    "direction":"{direction}",
                    "text":"{message}"
                }}
            """.format(
                date = random.randint(min_time, max_time),
                direction = getRandomDirection(), 
                message = message
            )

# Returns randomly generated conversation.
def generateConversation():
    random_message = getMessage(10)
    hasUnread = getRandomFlag() if len(random_message) > 0 else "false"

    return  """
    {{
        "identifier":"{uuid}",
        "hasUnreadMessages":{unread},
        "contact": 
            {{ 
                "firstName":"{first_name}",
                "lastName":"{last_name}",
                "isOnline":{online}
            }},
        "messages": [ {messages}
            ]
    }},
    """.format(
        uuid = uuid.uuid4(), 
        unread = hasUnread,
        first_name = random.choice(first_names),
        last_name = random.choice(last_names),
        online = getRandomFlag(),
        messages = random_message
    )

# Main routine

if __name__ == '__main__':
    json = "[\n"
    for index in range(args.count):
        json += generateConversation()
    json += "\n]"
    json = json[::-1].replace(",","",1)[::-1]

    with open(args.path, "w") as file:
        file.write(json)
