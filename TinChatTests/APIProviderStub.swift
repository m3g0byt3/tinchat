//
//  APIProviderStub.swift
//  TinChatTests
//
//  Created by m3g0byt3 on 09/12/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//

import Foundation
@testable import TinChat

final class APIProviderStub<T>: APIProviderProtocol where T: URLRepresentable {

    // MARK: - Public properties

    var performRequestCallsCount = 0

    // MARK: - Private properties

    private let stubData: Data?
    private let stubTimeout: TimeInterval

    // MARK: - Initialization/Deinitialization

    init(stubData: Data?, stubTimeout: TimeInterval = 3.0) {
        self.stubData = stubData
        self.stubTimeout = stubTimeout
    }

    // MARK: - Public API

    @discardableResult
    func performRequest(_ type: T, completion: @escaping Completion) -> NetworkToken? {
        performRequestCallsCount += 1
        // Emulate network delays
        DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + self.stubTimeout) {
            if let stubData = self.stubData {
                completion(.success(stubData))
            } else {
                completion(.failure(.unknown))
            }
        }

        return nil
    }
}
