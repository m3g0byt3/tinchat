//
//  ShibeOnlineServiceTests.swift
//  ShibeOnlineServiceTests
//
//  Created by m3g0byt3 on 09/12/2018.
//  Copyright © 2018 m3g0byt3. All rights reserved.
//
// swiftlint:disable implicitly_unwrapped_optional force_unwrapping

import Foundation
import XCTest
@testable import TinChat

/// Unit tests for `ShibeOnlineService` service.
final class ShibeOnlineServiceTests: XCTestCase {
    var service: PhotoServiceProtocol!
    var provider: APIProviderStub<ShibeOnlineAPI>!
    var referenceImage: UIImage!

    override func setUp() {
        super.setUp()
        referenceImage = R.image.photoPlaceholder()

        let stubData = UIImagePNGRepresentation(referenceImage)
        let provider = APIProviderStub<ShibeOnlineAPI>(stubData: stubData, stubTimeout: 3.0)
        let decoder = JSONDecoder()
        let modelParser = CodableJSONParser<[Photo]>(decoder: decoder)
        let photoParser = PhotoParser()
        let service = ShibeOnlineService(provider: provider, modelParser: modelParser, photoParser: photoParser)

        self.provider = provider
        self.service = service
    }

    override func tearDown() {
        referenceImage = nil
        provider = nil
        service = nil
        super.tearDown()
    }

    func testIsProviderBeingCalledByService() {
        // Given
        let completion: (Result<[Photo], NetworkError>) -> Void = { _ in }
        // When
        service.getPhotos(completion)
        // Then
        XCTAssertGreaterThan(provider.performRequestCallsCount, 0, "Service did not call provider")
    }

    func testIsServiceReturnCorrectImage() {
        // Given
        let expectation = XCTestExpectation(description: "Image downloading")
        let url = URL(string: "about:blank")!
        let photo = Photo(url: url)
        // When
        service.downloadPhoto(photo) { result in
            switch result {
            case .success(let model):
                XCTAssertNotNil(model.image, "Received invalid response")
                let imageBlob = UIImagePNGRepresentation(model.image!)
                let referenceImageBlob = UIImagePNGRepresentation(self.referenceImage)
                // We are comparing underlying `Data` blobs instead of actual `UIImage` instances because
                // direct comparison of `UIImage` instances is not recommend and may return unpredictable results
                XCTAssertEqual(imageBlob, referenceImageBlob, "Received incorrect image")
                expectation.fulfill()
            case .failure(let error):
                XCTAssert(false, "An error received: \(error.localizedDescription)")
            }
        }
        // Then
        wait(for: [expectation], timeout: 10.0)
    }
}
