# TinChat - TFS18F iOS educational project



Description
----------------
Peer-to-peer chat based on [MultipeerConnectivity](https://developer.apple.com/documentation/multipeerconnectivity) framework.
  


Requirements
----------------
* iOS 10.0+
* Xcode 9.4+
* Swift 4.1+



Installation
----------------
1. `cd` to the project directory
2. Install required pods: `pod install`
3. Open `TinChat.xcworkspace`
4. Build and run project at least twice (first run may fail due to R.swift resource and mocks generation)



Author
----------------
[m3g0byt3](https://github.com/m3g0byt3)



License
----------------
Licensed under a [MIT](https://opensource.org/licenses/MIT) license. See [LICENSE](LICENSE) for more information.